<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('product_image');
            $table->string('name',150);
            $table->longText('description');
            $table->string('vertus');
            $table->string('composition');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tarifs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('article_id');
            $table->foreignId('variation_id');
            $table->decimal('price',10,2);
            $table->foreign('article_id')
                ->references('id')
                ->on('articles')
                ->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('variation_id')
                ->references('id')
                ->on('variations')
                ->onUpdate('restrict')->onDelete('restrict');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('ligne_commandes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tarif_id');
            $table->foreignId('commande_id');
            $table->integer('quantite');
            $table->foreign('tarif_id')
                ->references('id')
                ->on('tarifs')
                ->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('commande_id')
                ->references('id')
                ->on('commandes')
                ->onUpdate('restrict')->onDelete('restrict');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifs');
        Schema::dropIfExists('ligne_commandes');
        Schema::dropIfExists('articles');
    }
}
