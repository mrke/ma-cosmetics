<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('email',120)->nullable();
            $table->string('name',60)->default('Anomym');
            $table->unsignedFloat('note',2,2);
            $table->text('message');
            $table->foreignId('article_id');
            $table->foreignId('user_id')->nullable();
            $table->foreign('article_id')
                ->references('id')
                ->on('articles')
                ->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')->onUpdate('restrict');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
