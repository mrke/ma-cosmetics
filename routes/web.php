<?php

use App\Http\Controllers\AdminController;
use App\Http\Livewire\Forgot;
use App\Http\Livewire\Login;
use App\Http\Livewire\ManageOrders;
use App\Http\Livewire\PointVentes;
use App\Http\Livewire\Products;
use App\Http\Livewire\Register;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShoppingController;
use App\Http\Livewire\Clients;
use App\Http\Livewire\Commentaires;
use App\Http\Livewire\Contact;
use App\Http\Livewire\ShippingInfos;
use App\Http\Livewire\ShowProduct;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\ManageProducts;
use App\Http\Livewire\Messages;
use App\Http\Livewire\Settings;
use App\Http\Livewire\Shopping\AddToCart;
use App\Http\Livewire\Shopping\FinalizeOrder;
use App\Http\Livewire\Shopping\MyCart;
use App\Http\Livewire\Users;


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/qui-sommes-nous', [HomeController::class, 'about'])->name('about');
Route::post('emailToNewsletter', [HomeController::class, 'storeEmail'])->name('storeEmail');

Route::group(['prefix' => 'nos-produits'], function (){
    Route::get('/', Products::class)->name('product');

    Route::get('/details/{id}/{name}',ShowProduct::class)
        ->where(['name'=>'.*','id'=>'[0-9]+'])->name('product.details');
});

// SHOPPING ROUTES
Route::get('product/{id}/ajouter-au-panier', AddToCart::class)->name('shopping.add');
Route::post('add-to-cart/store', [ShoppingController::class, 'addItemToCart'])->name('shopping.storeItem');
Route::get('mon-panier', MyCart::class)->name('shopping.myCart');

// OTHER ROUTES
Route::get('/nos-points-de-ventes', PointVentes::class)->name('pvente');
Route::get('/contactez-nous', Contact::class)->name('contact');

// AUTH ROUTES
Route::get('connexion', Login::class)->name('login');
Route::get('inscription', Register::class)->name('register');
Route::get('mot-de-passe', Forgot::class)->name('password');

// DASHBOARD

Route::group(['middleware' => 'auth:web'], function() {
    Route::get('information-de-livraison', ShippingInfos::class)->name('infos.shipping');
    Route::post('storeCustomer', [AdminController::class, 'storeShippingInfos'])->name('store.client');
    Route::group(['prefix' => '{role}'], function () {
        Route::get('dashboard', Dashboard::class)->name('dashboard');
        Route::get('produits', ManageProducts::class)->name('admin.products');
        Route::get('produits/nouveau/{product_id?}', [AdminController::class, 'createProduct'])->name('product.create');
        Route::post('produits/store', [AdminController::class, 'storeProduct'])->name('product.store');
        Route::get('commandes', ManageOrders::class)->name('admin.orders');
        Route::get('clients', Clients::class)->name('admin.clients');
        Route::get('utilisateurs', Users::class)->name('admin.users');
        Route::get('messages', Messages::class)->name('admin.contacts');
        Route::get('commentaires', Commentaires::class)->name('admin.comments');
        Route::get('parametres', Settings::class)->name('admin.settings');
    });
    Route::get('logout', [AdminController::class, 'logout'])->name('logout');
    Route::get('finaliser-ma-commande', FinalizeOrder::class)->name('sent_order');
    Route::post('sent-order', [ShoppingController::class, 'storeCommande']);
    Route::get('commander/terminer/success', [ShoppingController::class, 'shopping_ok']);
});

