<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Newsletter;
use App\Models\Tarif;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
         $products = Article::query()->orderBy('created_at','DESC')->paginate(6);
         $tarifs = Tarif::query()
            ->join('variations','tarifs.variation_id','=','variations.id')
            ->select('tarifs.*',
                'variations.variation as variation')
            ->get();
        
        $comments = Comment::all();
        $users = User::all();
        return view('Home.index',[
        'products' => $products,
        'tarifs' => $tarifs,
        'comments' => $comments,
        'users' => $users
        ]);
    }

    public function about()
    {
        return view('about.index');
    }

    public function products()
    {
        return view('product.index');
    }

    public function pointV()
    {
        $pventes = DB::table('point_de_ventes')->paginate(10);
        return view('pVentes.index', [
            'pventes' => $pventes
        ]);
    }


    public function storeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', 'unique:newsletters']
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $newsl = Newsletter::create([
            'email' => $request->email
        ]);

        return response()->json($newsl);
    }
}
