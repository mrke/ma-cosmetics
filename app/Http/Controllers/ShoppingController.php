<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Client;
use App\Models\Commande;
use App\Models\LigneCommande;
use App\Models\Role;
use App\Models\Tarif;
use App\Models\User;
use App\Models\Variation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ShoppingController extends Controller
{
    public function addItemToCart(Request $request)
    {
        $validators = Validator::make($request->all(), [
            'variations' => 'required'
        ]);
   
        if($validators->fails()){
            return response()->json($validators->errors(), 422);
        }
        $table = [];
        for($i=0; $i<count($request->variations); $i++){
            $table[]=[
                'product_id' => $request->product_id,
                'produc_name' => Article::find($request->product_id)->name,
                'variation_id' => $request->variations[$i],
                'variation' => Variation::find($request->variations[$i])->variation,
                'quantity' => $request->input("quantity-".$request->variations[$i])
            ];
        }

        return response()->json($table);
        
    }

    public function storeCommande(Request $request)
    {
        $client = Client::where('user_id', '=', Auth::id())->first();

        $commande = Commande::create([
            'client_id' => $client->id
        ]);

          $orderData = $request->ordered;

          for ($i=0; $i<count($orderData); $i++){
              LigneCommande::create([
                  'tarif_id' => Tarif::query()
                      ->where('variation_id','=',$orderData[$i]['variation_id'])
                      ->first()->id,
                  'commande_id' => $commande->id,
                  'quantite' => $orderData[$i]['quantity']
              ]);
          }

          return response()->json($client);
    }

    public function shopping_ok()
    {
        return view('orer-success');
    }
}
