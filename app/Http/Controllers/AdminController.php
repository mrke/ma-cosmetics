<?php

namespace App\Http\Controllers;

use App\Http\Repositories\AppRepository;
use App\Models\Article;
use App\Models\Client;
use App\Models\Tarif;
use App\Models\Variation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function createProduct($role, $id = null)
    {
      $variations = Variation::all();
      return is_null($id) ?
       view('admin.add-product', [
           'variations' => $variations,
           'product_id' => $id
       ])
       :
        view('admin.add-product', [
            'variations' => $variations,
            'product' => Article::find($id),
            'product_id' => $id,
            'tarifs' => Tarif::query()->join('variations','tarifs.variation_id','=','variations.id')
                ->select('tarifs.*','variations.variation as variation')
                ->where('tarifs.article_id', '=',$id)
                ->get()
        ]);
    }
    public function storeProduct(Request $request)
    {
        $validator = null;
        if (!is_null($request->product_image)) {
            $validator = Validator::make($request->all(), [
                'name' => ['required','string'],
                'description' => ['required','string'],
                'composition' => ['required','string'],
                'vertus' => ['required','string'],
                'product_image' => ['required','mimes:jpg,jpeg,png','image']
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => ['required','string'],
                'description' => ['required','string'],
                'composition' => ['required','string'],
                'vertus' => ['required','string'],
            ]);
        }

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (empty($request->id)) {
            $image = AppRepository::uploads($request->file('product_image'),NULL,'products');

            $article = Article::create([
                'product_image' => $image,
                'name' => $request->name,
                'description' => $request->description,
                'vertus' => $request->vertus,
                'composition' => $request->composition
            ]);

            if ($article) {
                for ($i=0; $i<count($request->variations); $i++){
                    Tarif::create([
                        'article_id' => $article->id,
                        'variation_id' => $request->variations[$i],
                        'price' => $request->input('tarif-'.$request->variations[$i])
                    ]);
                }
            }

            return response()->json($article);
        }
        else{
            $product = Article::find($request->id);


            $uploadImg = null;
            if (count(explode('/', $product->product_image)) > 1 && !is_null($request->product_image)) {
                $exImage = explode('/', $product->product_image)[6];
                $uploadImg = AppRepository::uploads($request->file('product_image'),$exImage,'products');
            }

            $product->name = $request->name;
            $product->description = $request->description;
            $product->composition = $request->composition;
            $product->vertus = $request->vertus;

            if (!is_null($uploadImg)) {
                $product->product_image = $uploadImg;
            }

            $tarif = Tarif::where('tarifs.article_id','=',$request->id)->first();

            if ($product->save()) {
                for ($i=0; $i<count($request->variations); $i++){
                    if ($tarif->variation_id == $request->variations[$i]) {
                        DB::table('tarifs')
                            ->where('tarifs.id','=',$tarif->id)
                            ->update([
                                'price' => $request->input('tarif-'.$request->variations[$i])
                            ]);
                    }
                }
            }

            return response()->json($product);
        }

    }

    public function logout()
    {
        Auth::logout();
        session()->flash('logout', 'Déconnexion effectuée avec succès');
        return redirect('/');
    }

    public function storeShippingInfos(Request $request)
    {
       $v = $request->validate([
        'firstName' => ['required'],
        'phone' => ['required'],
        'country' => ['required'],
        'city' => ['required'],
        ]);

        

       $client = Client::create([
            'firstName'=> $request->firstName,
            'lastname' => $request->lastName,
            'phone' => $request->phone,
            'country' => $request->country,
            'town' => $request->city,
            'address' => $request->address,
            'user_id' => Auth::id()
        ]);
        
        return 
        
        isset($_GET['type_register']) ?

        redirect()
        ->route('shopping.myCart'):

        redirect()
        ->route('home')
        ->with('register_successed','Votre compte chez Marie Andréa Cosmétics est un succès, vous pouvez profiter de nos services.');
    }
}
