<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Commentaires extends Component
{
    public function render()
    {
        $commentaire = Comment::query()
            ->join('articles','articles.id','=','comments.article_id')
            ->select('articles.*','comments.name as user_name')
            ->groupBy('comments.id')
            ->orderBy('comments.created_at','DESC')
            ->get();

        return view('livewire.commentaires', [
            'comments' => $commentaire
        ])
            ->layout('Layouts.admin-layout')
            ->slot('content');
    }
}
