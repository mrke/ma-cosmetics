<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class PointVentes extends Component
{
    use WithPagination;

    public function render()
    {
        $pventes = DB::table('point_de_ventes')->paginate(10);

    
        return view('livewire.point-ventes',[
            'pventes' => $pventes
        ])
            ->layout('Layouts.macosmetics')
            ->slot('content');
    }
}
