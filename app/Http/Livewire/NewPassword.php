<?php

namespace App\Http\Livewire;

use Livewire\Component;

class NewPassword extends Component
{
    public function render()
    {
        return view('livewire.new-password');
    }
}
