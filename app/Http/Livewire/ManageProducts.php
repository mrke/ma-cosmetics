<?php

namespace App\Http\Livewire;

use App\Models\Article;
use App\Models\Tarif;
use Livewire\Component;

class ManageProducts extends Component
{

    public function render()
    {
        $articles = Article::latest()->get();
        $tarifs = Tarif::query()->join('variations','tarifs.variation_id','=','variations.id')
            ->select('tarifs.*','variations.variation as variation')
            ->get();

        return view('livewire.manage-products', [
            'products' => $articles,
            'tarifs' => $tarifs
        ])
        ->layout('Layouts.admin-layout')
        ->slot('content');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        session()->flash('deleted', "$article->name a été supprimé avec succès !");
    }

}
