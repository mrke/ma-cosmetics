<?php

namespace App\Http\Livewire;

use App\Models\Article;
use App\Models\Tarif;
use Livewire\Component;
use Livewire\WithPagination;

class Products extends Component
{
    use WithPagination;
    public function render()
    {
        $products = Article::query()->orderBy('created_at','DESC')->paginate(10);
        $tarifs = Tarif::query()
            ->join('variations','tarifs.variation_id','=','variations.id')
            ->select('tarifs.*',
                'variations.variation as variation')
            ->get();

        return view('livewire.products',[
            'products' => $products,
            'tarifs' => $tarifs
        ])->layout('Layouts.macosmetics')->slot('content');
    }
}
