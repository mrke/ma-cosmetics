<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Register extends Component
{
    public $fullname;
    public $email;
    public $password;
    public $password_confirmation;
    public $next_older;

    protected $rules =  [
        'email' => ['required', 'email', 'unique:users'],
        'password' => ['required', 'min:6', 'confirmed']
    ];

    protected $messages = [
        'email.required' => 'le champ adresse email est obligatoire.',
        'email.email' => 'Entrer une adresse email valide.',
        'email.unique' => 'Cette adresse email existe déjà.',
        'password.required' => 'Le mot de passe est obligatoire.',
        'password.min' => 'Mot de passe trop court, 6 caractère minimum requis.',
        'password.confirmed' => 'Pas de correspondance entre les deux mots de passe, Merci de vérifier.'
    ];


    public function render()
    {
        return view('livewire.register')
        ->layout('Layouts.auth-layout')
        ->slot('content');
    }

    public function mount()
    {
       if(isset($_GET['type_register'])){
           $this->next_older = 'commande-suivant';
       }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {
      
     $this->validate();

     Auth::login( User::create([
        'fullname' => $this->fullname,
        'email' => $this->email,
        'password' => Hash::make($this->password),
        'role_id' => 2
      ]));
    
      return $this->next_older ? 
      redirect()->route('infos.shipping',['type_register'=>'commande-suivant']) : 
      redirect()->route('infos.shipping');


    }
}
