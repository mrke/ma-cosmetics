<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Users extends Component
{
    public function render()
    {
        $users = User::query()->join('roles','users.role_id','=','roles.id')
            ->select('users.*','roles.role as role')
            ->orderBy('users.created_at')
            ->where('users.id','!=',Auth::id())
            ->get();

        return view('livewire.users', [
            'users' => $users
        ])
            ->layout('Layouts.admin-layout')
            ->slot('content');
    }

}
