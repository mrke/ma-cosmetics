<?php

namespace App\Http\Livewire;

use Livewire\Component;


class Contact extends Component
{
    public $name;
    public $email;
    public $subject;
    public $message;

    protected $rules = [
        'name' => ['required'],
        'email' => ['required','email'],
        'subject' => ['required'],
        'message' => ['required','min:50','max:250']
    ];

    protected $messages = [
        'name.required' => 'Entrez votre nom complet',
        'email.required' => 'Entrez votre email',
        'email.email' => 'Entrez une adresse email valide',
        'subject.required' => 'Précisez l\'objet de votre message',
        'message.required' => 'Ecrivez votre message',
        'message.min' => 'Le message doit être au minimum 50 caractères',
        'message.max' => 'Le message ne doit pas exceder 250 caractères'
    ];


    public function render()
    {
        return view('livewire.contact')
        ->layout('Layouts.macosmetics')
            ->slot('content');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function sendMessage()
    {
        $this->validate();

        \App\Models\Contact::create([
            'fullName' => $this->name,
            'email' => $this->email,
            'object' => $this->subject,
            'message' => $this->message
        ]);

        session()->flash('message_sent', 'OK');
    }

    
}
