<?php

namespace App\Http\Livewire;

use App\Models\Contact;
use Livewire\Component;

class Messages extends Component
{
    public function render()
    {
        $contacts = Contact::latest()->get();

        return view('livewire.messages', [
            'contacts' => $contacts
        ])
            ->layout('Layouts.admin-layout')
            ->slot('content');
    }
}
