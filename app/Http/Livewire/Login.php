<?php

namespace App\Http\Livewire;

use App\Models\Client;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;


class Login extends Component
{
    public $email;
    public $password;
    public $next_older;

    protected $rules = [
        'email' => ['required', 'email'],
        'password' => ['required', 'min:6']
    ];

    protected $messages = [
        'email.required' => 'le champ adresse email est obligatoire.',
        'email.email' => 'Entrer une adresse email valide.',
        'password.required' => 'Le mot de passe est obligatoire.',
        'password.min' => 'Mot de passe trop court, 6 caractère minimum requis.'
    ];

    public function mount()
    {

       if(Auth::check() && isset($_GET['next_order'])) {
           if(Client::where('user_id','=',Auth::id())->first()){
                return redirect()->route('sent_order');
           } else {
            session()->flash('complete_profile', 'Veuillez completer vos information et pour finaliser votre commande.');
            return redirect()->route('infos.shipping');
           }
       } elseif(Auth::check()) {
        return redirect()->route('home');
       }
       
    }

    public function render()
    {
        return view('livewire.login')
            ->layout('Layouts.auth-layout')
            ->slot('content');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function login()
    {
        $this->validate();

        if(!Auth::attempt(['email' => $this->email, 'password' => $this->password])){
            session()->flash('logged_failed', 'Désolé, vos identifiants de connexion sont incorrects.');
            return redirect()->route('login');
        }

        $role = Role::find(Auth::user()->role_id);

        if($role->role === "customer" && is_null(Client::where('user_id',Auth::id())->first())){
            session()->flash('complete_profile', 'Veuillez completer vos information.');
            return redirect()->route('infos.shipping');
        }

        session()->flash('logged', 'Bienvenue, heureux de vous revoir');

        return redirect()->route('dashboard',['role'=>$role->role]);

    }


}
