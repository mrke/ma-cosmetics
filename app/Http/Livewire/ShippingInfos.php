<?php

namespace App\Http\Livewire;

use App\Models\Client;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ShippingInfos extends Component
{

    public function render()
    {
        return view('livewire.shipping-infos')
        ->layout('Layouts.auth-layout')
        ->slot('content');
    }
}
