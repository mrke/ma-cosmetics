<?php

namespace App\Http\Livewire;

use App\Models\Client;
use App\Models\Commande;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Clients extends Component
{
    public function render()
    {
        $clients = Client::query()
            ->join('users','users.id','=','clients.user_id')
            ->select('clients.*','users.avatar as avatar')
            ->orderBy('created_at','DESC')
            ->get();

        return view('livewire.clients',[
            'customers' => $clients
        ])
            ->layout('Layouts.admin-layout')
            ->slot('content');
    }
}
