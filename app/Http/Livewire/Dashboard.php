<?php

namespace App\Http\Livewire;

use App\Models\Client;
use Livewire\Component;
use App\Models\Commande;
use App\Models\LigneCommande;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\WithPagination;

class Dashboard extends Component
{
    use WithPagination;
    public function render()
    {

        if(Role::find(Auth::user()->role_id)->role === 'customer'){
            $commandes = Commande::query()
            ->join('ligne_commandes','ligne_commandes.commande_id', '=', 'commandes.id')
            ->join('tarifs','tarifs.id','=','ligne_commandes.tarif_id')
            ->select('commandes.*',
            DB::raw('SUM(ligne_commandes.quantite) as quantity'),
            DB::raw('SUM(tarifs.price * ligne_commandes.quantite) as montant'))
            ->groupBy('commandes.id')
            ->where('commandes.client_id','=',Client::where('user_id',Auth::id())->first()->id)
            ->orderBy('commandes.created_at', 'DESC')
            ->paginate(10);

        $ligneCommades = LigneCommande::query()
            ->join('tarifs','tarifs.id','=','ligne_commandes.tarif_id')
            ->join('variations','variations.id','=','tarifs.variation_id')
            ->join('articles','tarifs.article_id','=','articles.id')
            ->select('ligne_commandes.*',
            'tarifs.price','tarifs.article_id as article_id','tarifs.variation_id as variation_id',
            'variations.variation as variation',
            'articles.name as article_name')
            ->groupBy('ligne_commandes.id')
            ->get();

            return view('livewire.dashboard', [
                'commandes' => $commandes,
                'ligneCommandes' => $ligneCommades
            ])->layout('Layouts.admin-layout')
            ->slot('content');
        } 


        return view('livewire.dashboard')
            ->layout('Layouts.admin-layout')
            ->slot('content');
    }
}
