<?php

namespace App\Http\Livewire;

use App\Models\Client;
use App\Models\Commande;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ManageOrders extends Component
{
    public function render()
    {
        $orders = Commande::query()
            ->join('ligne_commandes','ligne_commandes.commande_id', '=', 'commandes.id')
            ->join('tarifs','tarifs.id','=','ligne_commandes.tarif_id')
            ->join('clients','commandes.client_id','=','clients.id')
            ->select('commandes.*',
                DB::raw('SUM(ligne_commandes.quantite) as quantity'),
                DB::raw('SUM(tarifs.price * ligne_commandes.quantite) as montant'),
                'clients.firstName as firstname', 'clients.lastName as lastname'
            )
            ->groupBy('commandes.id')
            ->orderBy('commandes.created_at', 'DESC')
            ->get();

        return view('livewire.manage-orders', [
            'orders' => $orders
        ])
        ->layout('Layouts.admin-layout')
        ->slot('content');
    }
}
