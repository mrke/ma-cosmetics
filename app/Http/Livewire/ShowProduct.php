<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Tarif;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;

class ShowProduct extends Component
{
    use WithPagination;
    public $product;
    public $products;
    public $tarifs;
    public $comments;

    public $fullname;
    public $email;
    public $message;

    public $q;

    protected $rules = [
        'email' => ['required','email'],
        'message' => ['required']
    ];

    protected $messages = [
        'email.required' => 'Veuillez fournir une adresse e-mail',
        'email.email' => 'Veuillez entrer une adresse e-mail valide',
        'message' => 'Veuillez écrire votre commentaire'
    ];


    public function render()
    {
        return view('livewire.show-product')
        ->layout('Layouts.macosmetics')
        ->slot('content');
    }

    public function mount($id)
    {
        
        $this->product = Article::find($id);

        $this->products = Article::where('id','!=',$id)->get();
        
         
        $this->comments = Comment::where('article_id','=',$id)
           ->orderBy('created_at','DESC')
            ->get();
        
        //dd($data);
        
        $this->tarifs = Tarif::query()->join('variations','tarifs.variation_id','=','variations.id')
            ->select('tarifs.*','variations.variation as variation')
            ->where('tarifs.article_id','=',$id)
            ->get();
       
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function addComment()
    {
        $this->validate();

        Comment::create([
            'email' => $this->email,
            'name' => $this->fullname,
            'message' => $this->message,
            'user_id' => Auth::check() ? Auth::id() : NULL,
            'article_id' => $this->product->id
        ]);

        session()->flash('commented', 'Commentaire ajouté');

        $this->comments = Comment::where('article_id','=',$this->product->id)
        ->orderBy('created_at','DESC')
         ->get();
    }


    public function searchProduct()
    {
        $this->products = Article::where('id','!=',$this->product->id)
        ->where('name','LIKE',"%$this->q%")
        ->get();   
    }
}
