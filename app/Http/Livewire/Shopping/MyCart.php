<?php

namespace App\Http\Livewire\Shopping;

use Livewire\Component;

class MyCart extends Component
{
    public function render()
    {
        return view('livewire.shopping.my-cart')
        ->layout('Layouts.macosmetics')
        ->slot('content');
    }
}
