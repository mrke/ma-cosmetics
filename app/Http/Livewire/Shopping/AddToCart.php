<?php

namespace App\Http\Livewire\Shopping;

use App\Models\Article;
use App\Models\Tarif;
use Livewire\Component;

class AddToCart extends Component
{
    public $product;
    public $tarifs;
    public $products;
    public $q;

    // STORE ATTRIBUTE
     
    public function render()
    {
        return view('livewire.shopping.add-to-cart')
        ->layout('Layouts.macosmetics')
        ->slot('content');
    }

    public function mount($id) {

        if(is_null($id)){
            session()->flash('product_null','Veillez selectionner un produit.');
            return redirect()->route('product');
        }

        $this->product = Article::find($id);

        $this->tarifs = Tarif::query()->join('variations','tarifs.variation_id','=','variations.id')
        ->select('tarifs.*','variations.variation as variation')
        ->where('tarifs.article_id','=',$id)
        ->get();

        $this->products = Article::where('id','!=',$id)->get();
    }

    public function searchProduct()
    {
        $this->products = Article::where('id','!=',$this->product->id)
        ->where('name','LIKE',"%$this->q%")
        ->get();   
    }
}
