<?php

namespace App\Http\Livewire\Shopping;

use App\Models\Client;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class FinalizeOrder extends Component
{
    public function render()
    {
        $client = Client::where('user_id','=', Auth::id())->first();
        return view('livewire.shopping.finalize-order', [
            'client' => $client
        ])
        ->layout('Layouts.macosmetics')
        ->slot('content');
    }
}
