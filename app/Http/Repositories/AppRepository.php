<?php


namespace App\Http\Repositories;


use App\Models\Client;
use App\Models\Comment;
use Carbon\Carbon;
use Carbon\Traits\Creator;
use JetBrains\PhpStorm\NoReturn;

class AppRepository
{

    public static function uploads($file,$exFile,$uploadType)
    {
        $baseURL = 'http://'.$_SERVER['HTTP_HOST']."/assets/uploads/";
        $fileName = null;

        for ($i=0; $i<6; $i++){
            $fileName .= mt_rand(1,9);
        }

        $uploadFile = $fileName.'.'.$file->extension();

        if (file_exists(public_path("assets/uploads/$uploadType/".$exFile) && !is_null($exFile))) {
            unlink(public_path("assets/uploads/$uploadType/".$exFile));
        }

        $file->move(public_path("/assets/uploads/$uploadType/"),$uploadFile);

        return $baseURL."$uploadType/".$uploadFile;

    }

    public static function triePrice($tarifs, $article_id)
    {
       $data = [];

       foreach ($tarifs as $tarif) {
           if ($tarif->article_id === $article_id) {
               $data[] = $tarif->price;
           }
       }

       $values = $data;


      for ($i=0; $i < count($values); $i++) {
          if ($values[$i] > $values[$i+1]) {
              $tmp = $values[$i+1];
              $values[$i+1] = $values[$i];
              $values[$i] = $tmp;
          }
          $i +=1;
      }
      return $values[0].' - '.$values[count($data)-1].' F CFA';
    }

    public static function formatURL($string)
    {
        $data = explode(' ',strtolower($string));
        $url = '';

        for ($i=0;$i<count($data); $i++){
            $url .= $data[$i].'-';
        }

        return substr($url,0,-1);
    }

    public static function keywordsGenerator($string)
    {
        $data = explode(' ',$string);

        $keywords = '';
        for ($i=0; $i<count($data); $i++){
            $keywords .= ', '.$data[$i];
        }

        return $keywords;
    }

    public static function gerenateNumber($date)
    {
        $val = explode(' ',$date);
        $vDate = '';
        $vH = '';



        for ($i=0; $i<count(explode('-',$val[0])); $i++){
            $vDate .= explode('-',$val[0])[$i];
        }

        for ($i=0; $i<count(explode(':',$val[1])); $i++){
            $vH .= explode(':',$val[1])[$i];
        }

        return $vDate.$vH;
    }

    public static function userInfos($user)
    {
        $client = Client::find($user->id);

        $fullname = '';

        if ($client) {
            $fullname = $client->firstName.' '.$client->lastName;
        } else {
            $fullname = $user->fullName;
        }

        return $fullname;
    }

    public static function commentByProductId(int $id) {
        $comments = Comment::query()->where('article_id','=',$id)->get();

        return count($comments) >= 10 ? count($comments) : '0'.count($comments);
    }

    public static function getDateFormat($date, $type)
    {
         $result = with(new Carbon($date))->locale('fr_FR');

        return $type === 'Hour'
            ? $result->isoFormat('HH:mm')
            : $result->isoFormat('DD MMM YYYY') ;
    }

    public static function formatDescription($description, $lenghth=null)
    {
        $val = '';
        if (is_null($lenghth)){
            $val = $description;
        } else {
            $val = substr($description, 0, 10).'...';
        }

        echo $val;
    }

    public static function formatStar($note)
    {
        $val = $note/5;
        $result = '';

        switch ($val) {
            case 0.2:
                $result = '<i class="fa fa-star text-yellow"></i>';
                break;
            case 0.3:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star-half text-yellow"></i>';
                break;
            case 0.4:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>';
                break;
            case 0.5:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star-half text-yellow"></i>';
                break;
            case 0.6:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>';
                break;
            case 0.7:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star-half text-yellow"></i>';
                break;
            case 0.8:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star- text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>';
                break;
            case 0.9:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star-half text-yellow"></i>';
                break;
            case 1:
                $result = '<i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>';
                break;
        }

        echo $result;
    }
}
