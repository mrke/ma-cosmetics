<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commande extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='commandes';
    protected $fillable =[
        'id','client_id','deleted_at','created_at','updated_at'
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function ligne_commandes()
    {
        return $this->hasMany('App\Models\LigneCommande');
    }
}
