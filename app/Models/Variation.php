<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='variations';
    protected $fillable =[
        'id', 'variation',  'deleted_at', 'created_at', 'updated_at'
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Models\Variation')
            ->using('App\Models\Tarif')
            ->withPivot('article_id','variation_id');
    }
}
