<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='clients';
    protected $fillable= [
        'id',
        'firstName',
        'lastName',
        'phone',
        'country',
        'town',
        'address',
        'user_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function commandes()
    {
        return $this->hasMany('App\Models\Commande');
    }
}
