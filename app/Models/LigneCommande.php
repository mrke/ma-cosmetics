<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LigneCommande extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='ligne_commandes';
    protected $fillable=[
        'id','tarif_id','commande_id','quantite','deleted_at','created_at','updated_at'
    ];

    public function article()
    {
        return $this->belongsTo('App\Models\Article');
    }

    public function commande()
    {
      return $this->belongsTo('App\Models\Commande');
    }
}
