<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tarif extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='tarifs';
    protected $fillable=[
        'id','article_id','variation_id','price','deleted_at','created_at','updated_at'
    ];
}
