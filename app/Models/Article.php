<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table ='articles';
    protected $fillable = [
        'id',
        'product_image',
        'name',
        'description',
        'vertus',
        'composition',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function variations()
    {
        return $this->belongsToMany('App\Models\Variation')
            ->using('App\Models\Tarif')
            ->withPivot('article_id','variation_id');
    }

    public function comments()
    {
        return  $this->hasMany('App\Models\Comment');
    }

    public function ligne_commandes()
    {
        return $this->hasMany('App\Models\LigneCommande');
    }


}
