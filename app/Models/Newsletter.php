<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='newsletters';
    protected $fillable=[
        'id','email','deleted_at','created_at','updated_at'
    ];
}
