<section id="testimonials" class="testimonials">
    <div class="container" data-aos="fade-up">
        <div class="section-title" data-aos="fade-up">
            <h2>Nos clients temoignes</h2>
            <p></p>
        </div>
        <div class="owl-carousel testimonials-carousel">
            @foreach ($comments as $comment)
            <div class="testimonial-item">
                @if(!is_null($comment->user_id))
                    @foreach ($users as $user)
                        @if($comment->user_id === $user->id)
                        <img src="{{$user->avatar ? $user->avatar : asset('assets/uploads/users/default.jpg')}}" class="testimonial-img" alt="Marie Andréa Cosmétics - {{$comment->name}}">
                        @endif
                    @endforeach
                @else
                <img src="{{asset('assets/uploads/users/default.jpg')}}" class="testimonial-img" alt="Marie Andréa Cosmétics - {{$comment->name}}">
                @endif
                <h3>{{$comment->name}}</h3>
                <h4>Client</h4>
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        {{$comment->message}}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
            </div>
            @endforeach
        </div>
    </div>
</section>
