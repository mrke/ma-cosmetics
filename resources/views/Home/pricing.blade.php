<section id="pricing" class="pricing">
    <div class="container">

        <div class="section-title">
            <h2 data-aos="fade-up">Tarification</h2>
            <p data-aos="fade-up">Etablissement des prix de nos savons selon la masse.</p>
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-6" data-aos="fade-up">
                <div class="box">
                    <h3>OFFRE 150 Grammes</h3>
                    <h4><sup>1,900</sup>F CFA<span> / Unité</span></h4>
                </div>
            </div>

            <div class="col-lg-3 col-md-6" data-aos="fade-up">
                <div class="box">
                    <h3>OFFRE 175 Grammes</h3>
                    <h4><sup>2,200</sup>F CFA<span> / Unité</span></h4>
                </div>
            </div>

            <div class="col-lg-3 col-md-6" data-aos="fade-up">
                <div class="box">
                    <h3>OFFRE 200 Grammes</h3>
                    <h4><sup>2,500</sup>F CFA<span> / Unité</span></h4>
                </div>
            </div>

            <div class="col-lg-3 col-md-6" data-aos="fade-up">
                <div class="box">
                    <h3>OFFRE 225 Grammes</h3>
                    <h4><sup>2,850</sup>F CFA<span> / Unité</span></h4>
                </div>
            </div>

        </div>
    </div>
</section>
