<section id="why-us" class="why-us">
    <div class="container">

        <div class="row">
            <div class="col-xl-4 col-lg-5" data-aos="fade-up">
                <div class="content">
                    <h3>
                        Pourquoi choisir Marie Andréa Cosmétics ?
                    </h3>
                    <p>
                        Les produits 100% bio, Les produits sans effets secondaires,
                        Les résultats sûrs et fiables, Nos produits sont à moindre coûts
                    </p>
                    <div class="text-center">
                        <a href="{{route('about')}}" class="more-btn">En savoir plus <i class="bx bx-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 d-flex">
                <div class="icon-boxes d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bx-cube-alt"></i>
                                <h4>Fabrication de savons 100% bio</h4>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bx-money"></i>
                                <h4>Vente en gros et en détails</h4>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bxs-shopping-bag"></i>
                                <h4>Commandes et livraisons savons 100% bio</h4>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
