<section id="clients" class="clients">
    <h3 class="section-title">Nos partenaires</h3>
    <div class="container" data-aos="fade-up">

        <div class="owl-carousel clients-carousel">
            <a href="https://www.dpci.ci/" target="_blank">
                <img src="{{asset('assets/img/dpci_logo.jpg')}}" alt="LOGO - DPCI">
            </a>
            <a href="https://cotedivoire.ubipharm.com/" target="_blank">
                <img src="{{asset('assets/img/ubipharm.png')}}" alt="LOGO - UBIPHARM Cote d'Ivoire">
            </a>
        </div>

    </div>
</section>
