<section class="service-details section-bg">
    <div class="container">
        <div class="section-title" data-aos="fade-up">
            <h2>Catalogue de savons - Marie Andréa Cosmétics</h2>
            <p></p>
        </div>
        <div class="row">
         @foreach($products as $product)
            <div class="col-md-4 d-flex align-items-stretch" data-aos="fade-up">
               <x-product-item :product="$product" :tarifs="$tarifs"/>
            </div>
         @endforeach
        </div>
    </div>
</section>
