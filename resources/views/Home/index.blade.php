<x-ma-cosmetics-layout>

    <x-slot name="title">Accueil</x-slot>

    <x-slot name="meta_description">
        Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>

    <x-slot name="home_active">active</x-slot>

    <x-slot name="banner">
        <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
            <div class="container" data-aos="fade-in">
                <h1>Bienvenue à Marie Andréa Cosmétics</h1>
                <h2>La beauté naturellement.</h2>
                <div class="d-flex align-items-center">
                    <i class="bx bxs-right-arrow-alt get-started-icon"></i>
                    <a href="{{route('contact')}}" class="btn-get-started scrollto">Prenez contact</a>
                </div>
            </div>
        </section>
    </x-slot>

    <x-slot name="content">
        @include('Home.why-us')
        @include('Home.about')
        @include('Home.partner')
        @include('Home.products')
        @include('Home.temoignage')
        @include('Home.pricing')
    </x-slot>

</x-ma-cosmetics-layout>
