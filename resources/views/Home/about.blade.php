<section id="about" class="about section-bg">
    <div class="container">

        <div class="row">
            <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch" data-aos="fade-right">

            </div>

            <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                <h4 data-aos="fade-up">Qui sommes nous</h4>
                <h3 data-aos="fade-up">Marie Andréa SARL</h3>
                <p data-aos="fade-up">
                    vous propose du savon artisanal sous toutes ses formes et des produits cosmétiques à base de produits naturels.
                </p>

                <div class="icon-box" data-aos="fade-up">
                    <div class="icon"><i class="bx bx-play"></i></div>
                    <h4 class="title"><a href="#">Concernant la fabrication</a></h4>
                    <p class="description">
                        Concernant la fabrication de nos savons artisanaux nous appliquons la saponification à froid, une méthode traditionnelle et artisanale permettant d’élaborer des savons uniques, riches en huile végétale (huile de palmiste et le beurre de karité), qui sont de véritables soins protecteurs pour votre peau et votre santé.
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>
