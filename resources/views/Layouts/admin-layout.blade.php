<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MARIE ANDREA COSMETICS | @yield('page')</title>
    @livewireStyles
    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{asset('assets/vendor/summernote/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}">
    <style type="text/css">
        .avatarForm {
            position: absolute;
            top: 60px;
            right: -10px;
            font-size: 12px;
            background: url({{asset('assets/icons/add.svg')}}) no-repeat center center / cover;
            width: 30px;
            height: 30px;
        }
        #avatar-input {
            width: 50px;
            height: 50px;
            opacity: 0;
        }
        .avatarForm > button[type="submit"] {
            position: absolute;
            top: -3px !important;
            right: -100px;
        }
    </style>

</head>
<body>
<div>
    <main class="main">
        <div class="sptb">
            <div class="container">
                <div class="row">
                    @if(session()->has('logged'))
                    <div class="col-lg-12">
                        <div class="alert alert-success">
                            {{session('logged')}}
                        </div>
                    </div>
                    @endif

                    @if(App\Models\Role::find(auth()->user()->role_id)->role === 'administrator')
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Mon Tableau de bord</h3>
                            </div>
                            <div
                                class="card-body text-center item-user border-bottom"
                            >
                                <div class="profile-pic">
                                    <div class="profile-pic-img">
                                        <span class="dots bg-success"></span>
                                        <img
                                            src="{{auth()->user()->avatar ? auth()->user()->avatar : asset('assets/uploads/users/default.jpg')}}"
                                            alt="ADMIN IMAGE"
                                            class="border-round"
                                            ref="displayAvatar"
                                        />
                                        <form
                                            class="avatarForm"
                                            action="#"
                                            enctype="multipart/form-data"
                                        >
                                            <input
                                                type="file"
                                                name="avatar"
                                                id="avatar-input"
                                                onchange="attachAvatar()"
                                            />
                                            <button
                                                disabled
                                                type="submit"
                                                class="btn btn-primary btn-sm"
                                            >
                                                Enregistrer
                                            </button>
                                        </form>
                                    </div>
                                    <a href="#" class="text-dark">
                                        <h4
                                            class="mt-3 mb-0 font-weight-semibold"
                                        >
                                            {{
                                            auth()->user()->fullName
                                            ?
                                            auth()->user()->fullName
                                            : 'ADMINISTRATEUR'
                                            }}
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="item-links mb-0">
                                <a
                                    href="{{route('dashboard',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$home_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-edit"></i>
                                    </span>
                                    Editer le profil
                                </a>
                                <a
                                    href="{{route('admin.products',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$p_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bx-cube"></i>
                                    </span>
                                    Produits
                                </a>
                                <a
                                    href="{{route('admin.orders',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$order_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-shopping-bags"></i>
                                    </span>
                                    commandes
                                </a>
                                <a
                                    href="{{route('admin.clients',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$clt_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-group"></i>
                                    </span>
                                    clients
                                </a>
                                <a
                                    href="{{route('admin.users',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$u_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-user-pin"></i>
                                    </span>
                                    Utilisateurs
                                </a>

                                <a
                                    href="{{route('admin.contacts',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$m_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-envelope"></i>
                                    </span>
                                    Messages
                                </a>
                                <a
                                    href="{{route('admin.comments',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$cmt_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-comment"></i>
                                    </span>
                                    Commentaires
                                </a>
                                <a
                                    href="{{route('admin.settings',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}"
                                    class="d-flex border-bottom {{$stg_active ?? ''}}"
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-cog"></i>
                                    </span>
                                    Paramètres
                                </a>
                                <a
                                    href="{{route('logout')}}"
                                    class="d-flex border-bottom "
                                >
                                    <span class="icon mr-2">
                                        <i class="bx bxs-log-out"></i>
                                    </span>
                                    Se déconnecter
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-9">
                        <div class="{{App\Models\Role::find(auth()->user()->role_id)->role === 'administrator' ? 'card' : ''}}">
                            {{ $content }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@livewireScripts

<script src="{{asset('assets/vendor/summernote/summernote-bs4.js')}}"></script>
<script src="{{asset('assets/vendor/summernote/lang/summernote-fr-FR.js')}}"></script>
<script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
<script src="{{asset('assets/vendor/select2/js/i18n/fr.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"
        integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/localization/messages_fr.min.js"
        integrity="sha256-WvIgGf8Z3j/ql0As1TVq4yrQ1JJ0voJirgWWkrfM8fc=" crossorigin="anonymous">
</script>
<script>
    function showDetails(id) {

let el = document.getElementById('details-'+id);

if (el.getAttribute('style') === 'display:none'){
    el.setAttribute('style','display:block');
} else {
    el.setAttribute('style','display:none')
}

}
</script>
</body>
</html>
