<?php session_start() ?>
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{'Marie Andréa Cosmétics | '.$title ?? 'Marie Andréa Cosmétics | Bienvenue'}}</title>
    <meta content="Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel" name="descriptison">
    <meta content="Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel" name="keywords">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    @livewireStyles
</head>
<body>
<div>
    <main class="main">
        <section class="sptb">
            <div class="container customer-page">
                <div class="row">
                    <div class="single-page">
                        <div class="center-block"></div>
                        <div class="col-lg-5 mx-auto d-block">
                            <div class="wrapper wrapper-2">
                                {{ $content }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

@livewireScripts
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"
        integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/localization/messages_fr.min.js"
        integrity="sha256-WvIgGf8Z3j/ql0As1TVq4yrQ1JJ0voJirgWWkrfM8fc=" crossorigin="anonymous"></script>
</body>
</html>

