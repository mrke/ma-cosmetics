<x-slot name="title">Créer un compte</x-slot>
<form wire:submit.prevent="store" autocomplete="off" method="POST" id="register" class="card-body" tabindex="500">
    @csrf
    <div class="name">
        <input type="text" wire:model="fullname" id="name" value="{{old('name')}}">
        <label for="name">Nom complet</label>
    </div>

    <div class="email">
        <input type="text" wire:model="email" id="email"  value="{{old('email')}}">
        <label for="email">Adresse E-mail</label>
    </div>
    @error('email')
        <span class="error">{{$message}}</span>
    @enderror


    <div class="password">
        <input type="password" wire:model="password" id="password">
        <label for="password">Mot de passe</label>
    </div>
    @error('password')
    <span class="error">{{$message}}</span>
    @enderror

    <div class="password_confirmation">
        <input type="password" wire:model="password_confirmation" id="password_confirmation">
        <label for="password_confirmation">Confirmer mot de passe</label>
    </div>

    <div class="submit">
        <button class="btn btn-primary btn-block" type="submit">
            S'inscrire
        </button>
    </div>

    <p class="text-dark mb-0">
        Vous avez déjà un compte?
        <a href="{{route('login')}}" class="text-primary ml-1">Se connecter</a>
    </p>
</form>