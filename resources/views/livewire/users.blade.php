<x-slot name="u_active">active</x-slot>
<div class="card-header">
    <h3 class="card-title">LISTE DES UTILISATEURS</h3>
</div>
<div class="card-body">
    <div class="content">
        <div class="table-responsive border-top">
            <table
                class="table table-bordered table-hover mb-0 text-nowrap"
                id="user-table"
            >
            <thead>
                <tr>
                    <th>USER</th>
                    <th>ROLE</th>
                    <th>INSCRIT LE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                   <tr>
                    <td>
                        <img class="user-avatar" src="{{$user->avatar ? $user->avatar : asset('assets/uploads/users/default.jpg')}}" alt="Marie Andréa Cosmétics - Photo utilisateur">
                        {{$user->fullName ? $user->fullName : 'PAS DE NOM'}}
                     </td>
                     <td>{{$user->role === 'customer' ? 'Client' : 'Administrateur'}}</td>
                     <td>{{$user->created_at}}</td>
                   </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#u-table").dataTable({
            responsive: true,
            processing: true,
            language: {
                url:  url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            }
        })
    })
</script>