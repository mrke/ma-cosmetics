<x-slot name="p_active">active</x-slot>
<div id="article">
    @if(session()->has('deleted'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="bx bxs-check-circle mr-2"></i> {{session('deleted')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card-header">
        <h3 class="card-title">Liste des produits</h3>
    </div>
    <div class="card-body">
        <div class="tabs-menu">
            <ul class="nav">
                <li>
                    <a href="{{route('product.create', ['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}" class="btn-sm btn-primary"
                            id="add-product-btn"
                    >
                        <i class="fa fa-plus mr-2"></i>Ajouter
                    </a>
                </li>
            </ul>
        </div>
        <div class="content">
            <div class="table-responsive border-top">
                <table
                    class="table table-bordered table-hover mb-0 text-nowrap"
                    id="product-table"
                >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>PRODUIT</th>
                        <th>COMMANDES</th>
                        <th>DATE</th>
                        <th>ACTIONS</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td></td>
                                <td>
                                    <div class="media mt-0 mb-0">
                                        <div class="media-body">
                                            <div class="card-item-desc p-0">
                                                <a href="#" class="text-dark">
                                                    <h4 class="font-weight-semibold">{{$product->name}}</h4>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-tag w-4"></i
                                                    >
                                                    {{$product->composition}}
                                                </a
                                                ><br />
                                                <a href="#">
                                                    <i class="fa fa-tag w-4"></i>
                                                    {{$product->vertus}}
                                                </a><br>

                                                @foreach($tarifs as $tarif)
                                                    @if ($tarif->article_id === $product->id)
                                                        <a href="#" class="d-block">
                                                            <i class="fa fa-arrow-alt-circle-right w-4"></i>
                                                            {{$tarif->variation}}&nbsp;:&nbsp;{{$tarif->price}} F CFA
                                                        </a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{
                                        count(\App\Models\Commande::join('ligne_commandes','ligne_commandes.commande_id','=','commandes.id')
                                        ->join('tarifs','ligne_commandes.tarif_id','=','tarifs.id')
                                        ->where('tarifs.article_id','=',$product->id)
                                        ->select('commandes.*')
                                        ->groupBy('commandes.id')
                                        ->get())
                                    }}
                                </td>
                                <td>
                                    {{$product->created_at}}
                                </td>
                                <td>
                                    <a
                                        class="btn btn-success btn-sm text-white"
                                        href="{{route('product.create',['role' => App\Models\Role::find(auth()->user()->role_id)->role, 'product_id' => $product->id])}}"
                                    >
                                        <i class="bx bxs-edit"></i>
                                    </a>

                                    <button
                                        class="btn btn-danger btn-sm text-white"
                                         wire:click.prevent="destroy({{$product->id}})"
                                    >
                                        <i class="bx bxs-trash"></i>
                                    </button>

                                    <a
                                        href="{{route('product.details',['name'=>\App\Http\Repositories\AppRepository::formatURL($product->name),'id'=>$product->id])}}"
                                        class="btn btn-primary btn-sm text-white"
                                    >
                                        <i class="bx bxs-eyedropper"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#product-table").dataTable({
            responsive: true,
            processing: true,
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            }
        })
    })
</script>
