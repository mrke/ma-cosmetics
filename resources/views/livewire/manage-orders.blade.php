<x-slot name="order_active">active</x-slot>
<div class="card-header">
    <h3 class="card-title">Liste des commandes</h3>
</div>
<div class="card-body">
    <div class="content">
        <div class="table-responsive border-top">
            <table
                class="table table-bordered table-hover mb-0 text-nowrap"
                id="orders-table"
            >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>COMMANDE</th>
                        <th>MONTANT</th>
                        <th>DATE</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($orders as $order)
                       <tr>
                           <td>{{'#'.App\Http\Repositories\AppRepository::gerenateNumber($order->created_at).$order->id}}</td>
                           <td>{{$order->firstname.' '.$order->lastname}}</td>
                           <td>{{$order->montant}} F CFA</td>
                           <td>{{$order->created_at}}</td>
                       </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#orders-table").dataTable({
            processing: true,
            responsive: true,
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            }
        })
    })
</script>
