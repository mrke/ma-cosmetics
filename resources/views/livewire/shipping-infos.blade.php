<x-slot name="title">Créer un compte, Informations de livraison</x-slot>
<h3 class="card-title" style="margin-bottom: 0">Vos informations de livraison</h3>
<form method="POST" action="{{route('store.client')}}" class="card-body" tabindex="500" autocomplete="off">
    @csrf
    <div class="password">
        <input value="{{old('firstName')}}" type="text" name="firstName" id="firstName" placeholder="Entrer prénom" >
        <label for="firstName">Prénom</label>
    </div>
   
    @error('firstname')
    <div class="error">
    {{$message}}
    </div>
    @enderror

    
    <div class="password">
        <input value="{{old('lastName')}}" type="text" name="lastName" id="lastName" placeholder="Entrer nom" >
        <label for="lastName">Nom</label>
    </div>

    <div class="password">
        <input value="{{old('country')}}" type="text" name="country" id="country" placeholder="Entrer pays" >
        <label for="country">Pays</label>
    </div>
    @error('country')
        <div class="error">
            {{$message}}
        </div>
    @enderror
    
    <div class="password">
        <input value="{{old('city')}}" type="text" name="city" id="city" placeholder="Entrer Ville" >
        <label for="town">Ville</label>
    </div>
    @error('city')
        <div class="error">
            {{$message}}
        </div>
    @enderror
 
    <div class="password">
        <input value="{{old('address')}}" type="address" name="address" id="address" placeholder="Entrer adresse">
        <label for="address">Adresse</label>
    </div>

    <div class="password">
        <input value="{{old('phone')}}" type="tel" name="phone" id="phone" placeholder="Entrer numéro de téléphone" >
        <label for="phone">Numéro de téléphone</label>
    </div>
    @error('phone')
        <div class="error">
            {{$message}}
        </div>
    @enderror

    <div class="submit">
        <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-check-circle-right mr-1"></i> Terminer mon inscription 
        </button>
    </div>
</form>

<script>
    !(function($) {
        'use strict'


    })
</script>
