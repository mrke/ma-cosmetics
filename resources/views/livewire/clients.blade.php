<x-slot name="clt_active">active</x-slot>
<div class="card-header">
    <h3 class="card-title">LISTE DES CLIENTS</h3>
</div>
<div class="card-body">
    <div class="content">
        <div class="table-responsive border-top">
            <table
                class="table table-bordered table-hover mb-0 text-nowrap"
                id="c-table"
            >
            <thead>
                <tr>
                    <th>CLIENTS</th>
                    <th>N.COMMANDES</th>
                    <th>INSCRIT LE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customers as $c)
                    <tr>
                        <td>
                            <img src="{{$c->avatar ? $c->avatar : asset('assets/uploads/users/default.jpg') }}" class="avatar avatar-md mr-2" alt="Marie Andréa Cosmétics - USER AVATAR">
                            {{$c->firstName.' '.$c->lastname}}
                        </td>
                        <td class="text-center">
                            {{
                                count(\App\Models\Commande::where('client_id',$c->id)->get()) < 10
                                ?
                                '0'.count(\App\Models\Commande::where('client_id',$c->id)->get())
                                :
                                count(\App\Models\Commande::where('client_id',$c->id)->get())
                            }}
                        </td>
                        <td>{{$c->created_at}}</td>
                    </tr>
                @endforeach
               
            </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#c-table").dataTable({
            responsive: true,
            processing: true,
            language: {
                url:  url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            }
        })
    })
</script>