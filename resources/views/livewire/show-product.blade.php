<div>
    <x-slot name="title">{{$product->name}}</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel, {{$product->name}}, {{$product->vertus}}
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, {{$product->name}}, {{$product->vertus}} beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page">Détails du produit</x-slot>
            <x-slot name="page_title">{{$product->name}}</x-slot>
        </x-other-page-banner>
    </x-slot>

    <section class="blog">
        <div class="container">
          <div class="row">
              <x-product-details :product="$product" :tarifs="$tarifs" :comments="$comments" />
              <x-show-product-side-bar :products="$products" :tarifs="$tarifs"/>
          </div>
        </div>
    </section>
</div>