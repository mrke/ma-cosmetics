<x-slot name="m_active">active</x-slot>
<div class="card-header">
    <h3 class="card-title">MESSAGES</h3>
</div>
<div class="card-body">
    <div class="content">
        <div class="table-responsive border-top">
            <table class="table table-bordered table-hover mb-0 text-nowrap" id="m-table">
                <thead>
                    <tr>
                        <th>MESSAGE</th>
                        <th>DATE</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($contacts as $message)
                       <tr>
                           <td>
                               <i class="bx bxs-envelope mr-1"></i>{{$message->object}} <br>
                               <i class="bx bxs-user mr-1"></i>{{$message->fullName}}
                           </td>
                           <td>{{$message->created_at}}</td>
                           <td>
                               <a href="#" class="btn btn-sm btn-info">
                                   <i class="bx bxs-eye mr-1"></i>Afficher 
                               </a>
                           </td>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>