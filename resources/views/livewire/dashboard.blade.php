@if(App\Models\Role::find(auth()->user()->role_id)->role === 'administrator')
<x-slot name="home_active">active</x-slot>
<div id="edit-profile">
    <div class="card-header">
        <h3 class="card-title">Editer le profil</h3>
    </div>
    <div class="card-body">
        <form autocomplete="off" action="#" id="admin">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label required">Prénoms</label>
                        <input type="text" class="form-control" name="prenom" placeholder="Prénom">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label required">Nom De Famille</label>
                        <input type="text" class="form-control" name="nom" placeholder="Nom De Famille">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label">Adresse Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Adresse Email">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label required">Fonction / Profession</label>
                        <input type="text" class="form-control" name="fonction" placeholder="Ex: Directeur G, Developpeur">
                    </div>
                </div>
                <div class="form-group text-center col-lg-12">
                    <button class="btn btn-primary" type="submit">
                        Sauvegarder maintenant
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
   @else
   <div class="section-title center-block text-center">
    <h2>TOUTES MES COMMANDES</h2>
</div>
 <div class="col-lg-12 commandes-list">
    <div class="card">
        <div class="card-body">
            <div class="commande-table table-responsive border-top">
                <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                    <thead>
                         <tr>
                             <th>COMMANDE</th>
                             <th>ETAT</th>
                             <th>DATE</th>
                             <th>TOTAL</th>
                             <th></th>
                         </tr>
                    </thead>
                    <tbody>
                    @foreach($commandes as $commande)
                    <tr>
                        <td>
                            {{'#'.App\Http\Repositories\AppRepository::gerenateNumber($commande->created_at).$commande->id}}
                        </td>
                        <td>
                            <a href="#" class="badge badge-success">
                                Terminer
                            </a>
                        </td>
                        <td>{{$commande->created_at}}</td>
                        <td>{{$commande->montant}} F CFA</td>
                        <td>
                            <button type="button" onclick="showDetails({{$commande->id}})" class="btn btn-primary btn-sm text-white">
                                <i class="bx bxs-eye"></i>
                            </button>
                        </td>
                    </tr>
                    <div class="row">
                        <div class="col-lg-5">
                            <ul class="ul-details" id="details-{{$commande->id}}" style="display: none">
                                @foreach($ligneCommandes as $value)
                                    @if($commande->id == $value->commande_id)
                                        <li class="item p-4">
                                            <div class="footerimg mt-0 mb-0">
                                                <div class="footerimg-l mb-0">
                                                    <a href="#" class="time-title p-0 leading-normal mt-2 mr-6">
                                                        {{$value->article_name}}
                                                    </a>
                                                    <a href="#" class="time-title p-0 leading-normal mt-2 mr-6">
                                                        {{$value->variation}}
                                                    </a>
                                                    <a href="#" ><span class="text-muted mr-1 mt-2">Qte : {{$value->quantite}}</span></a>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif