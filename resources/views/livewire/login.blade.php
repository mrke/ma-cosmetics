<x-slot name="title">Authentification</x-slot>
@if(session()->has('logged_failed'))
    <div class="alert alert-danger">
        {{session('logged_failed')}}
    </div>
@endif
<form wire:submit.prevent="login" autocomplete="off" class="card-body" tabindex="500">

@csrf
<input type="hidden" wire:model="next_older" value="{{ isset($_GET['type_login']) ? 'next_older' : ''}}">
<div class="email">
    <input type="email" wire:model="email" id="email" :value="old('email')">
    <label for="email">Adresse E-mail</label>
</div>
@error('email')
    <div class="error">{{$message}}</div>
@enderror

<div class="password">
    <input type="password" wire:model="password" id="password">
    <label for="password">Mot de passe</label>
</div>

@error('password')
    <div class="error">
        {{$message}}
    </div>
@enderror

<div class="submit">
    <button class="btn btn-primary btn-block">
        S'identifier
    </button>
</div>

<p class="mb-2">
    <a style="color: #75809c;" class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password') }}">
        {{ __('Mot de passe oublié ?') }}
    </a>
</p>

<p class="text-dark mb-0">
    Vous n'avez pas de compte?
    <a href="{{isset($_GET['next_order']) ? route('register',['type_register'=>'commande-suivant']) : route('register')}}" class="text-primary ml-1">S'inscrire</a>
</p>

</form>
