<x-slot name="title">Réinitialiser mon mot de passe</x-slot>
<div class="p-4 mb-5">
    <h4 class="pb-2">Mot de passe oublié</h4>

</div>
<form id="register" class="card-body" tabindex="500">
    @csrf
    <div class="email">
        <input type="email" name="email" id="email" value="{{old('email')}}" required>
        <label for="email">Adresse E-mail</label>
    </div>
    <div class="submit">
        <button class="btn btn-primary btn-block">
            Envoyer
        </button>
    </div>
    <div class="text-center text-dark mb-0">
        Oublie, <a href="{{route('login')}}">renvoyez-moi</a>
        à la connexion.
    </div>
</form>
