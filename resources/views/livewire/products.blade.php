<div>
    <x-slot name="title">Nos savons</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Catalogue des savons Marie Andréa Cosmétics</x-slot>
            <x-slot name="page_title">Nos savons</x-slot>
        </x-other-page-banner>
    </x-slot>
    
    <x-order-alert />
   
    <section class="service-details" style="padding-top: 0">
        @if (session()->has('register_successed'))
            <div class="alert alert-success">
                {{session('register_successed')}}
            </div>
        @endif
        <div class="container">
            <div class="row">
            @foreach($products as $product)
                <div class="col-md-4 d-flex align-items-stretch">
                   <x-product-item :product="$product" :tarifs="$tarifs"/>
                </div>
            @endforeach
            </div>
        </div>
    </section>
</div>
