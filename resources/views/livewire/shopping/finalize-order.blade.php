<x-slot name="title">Envoyer ma commande</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Catalogue des savons Marie Andréa Cosmétics</x-slot>
            <x-slot name="page_title">Finaliser ma commande</x-slot>
        </x-other-page-banner>
    </x-slot>

    <section class="blog">
        <div class="container">
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 entries">
                <article class="entry entry-single">
                    <h4>Mes informations</h4>
                    <hr>
                    <p>
                        <strong>Nom & Prénoms : </strong><span>{{$client->firstName.' '.$client->lastname}}</span><br>
                        <strong>Adresse : </strong>
                        <span>
                            {{$client->country.', '.$client->town.', '.$client->address}}
                        </span><br>
                        <strong>Téléphone : </strong>
                        <span>
                            {{$client->phone}}
                        </span><br>
                    </p>
                </article>
                <article class="entry entry-single">
                    <h4>Mes articles choisis</h4>
                    <table class="table" id="sent_order_table">
                       <thead>
                        <tr>
                            <th>PRODUIT</th>
                            <th>VARIATION</th>
                            <th>QUATITE</th>
                        </tr>
                       </thead>
                       <tbody>
                           <tr></tr>
                       </tbody>
                    </table>
                </article>
                <button id="sent-order-btn" class="btn btn-success">Envoyer ma commande</button>
            </div>
            <div class="col-lg-2"></div>
          </div>
        </div>
    </section>
