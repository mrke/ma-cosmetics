<x-slot name="title">Mon panier</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Catalogue des savons Marie Andréa Cosmétics</x-slot>
            <x-slot name="page_title">Mon panier de commande</x-slot>
        </x-other-page-banner>
    </x-slot>

    <section class="blog">
        <div class="container">
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 entries" id="my-cart-container">
                
            </div>
            <div class="col-lg-2"></div>
          </div>
        </div>
    </section>