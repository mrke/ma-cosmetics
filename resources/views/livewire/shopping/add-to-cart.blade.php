<div>
    <x-slot name="title">{{$product->name ?? 'Mon panier'}}</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel, {{$product->name ?? ''}}, {{$product->vertus ?? ''}}
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, {{$product->name ?? ''}}, {{$product->vertus ?? ''}} beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page">Initiation d'achat</x-slot>
            <x-slot name="page_title">{{$product->name ?? 'Aucun produit selectionné'}}</x-slot>
        </x-other-page-banner>
    </x-slot>

    <section class="blog">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 entries">
                <article class="entry entry-single">
                    <form action="javascript:void(0)" name="addToCartForm" id="addToCartForm">
                        @csrf
                        <input type="hidden" value="{{$product->id}}" name="product_id">
                        <div class="form-group">
                            <label class="form-label text-dark font-weight-semibold">Choisissez le poids et ajouter au panier</label>
                            <x-add-to-cart-item :variations="$tarifs" />
                        </div>
                        <div class="form-group text-center">
                            <a class="btn btn-danger" href="#">
                                <i class="bx bxs-window-close"></i>Abandonner
                            </a>
                            <button class="btn btn-primary" type="submit">
                                <i class="bx bxs-check-circle"></i>Valider
                            </button>
                        </div>
                    </form>
                </article>
            </div>
            <x-show-product-side-bar :products="$products" :tarifs="$tarifs"/>
          </div>
        </div>
    </section>
</div>
