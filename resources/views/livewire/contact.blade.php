<x-slot name="title">Contactez-nous</x-slot>

<x-slot name="meta_description">
    Comment nous contacter  ? cliquez ici et suivez les instruction...
    Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
</x-slot>

<x-slot name="meta_keywords">
    contact, savons bio moins cher, Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
</x-slot>
<x-slot name="contact_class">active</x-slot>

<x-slot name="banner">
    <x-other-page-banner>
        <x-slot name="page"> Catalogue des savons Marie Andréa Cosmétics</x-slot>
        <x-slot name="page_title">Nos savons</x-slot>
    </x-other-page-banner>
</x-slot>
<section id="contact" class="contact">
    <div class="container">

        <div class="section-title">
            <h2 data-aos="fade-up">Contact</h2>
            <p>
                Ouvert de 09h à 17h30. <br>
                Nous sommes situés aux 2 plateaux après le centre commercial sococe le prochain carrefour à droite dans la direction en allant vers angré en passant devant la boulangerie le Roi du pain, Marie Andréa SARL est à 150m, à la villa 45.
            </p>
        </div>

        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-4 mt-4">
                <div class="info-box">
                    <i class="bx bx-map"></i>
                    <h3>Notre Adresse</h3>
                    <p>
                        Abidjan II plateaux, après SOCOCE.
                    </p>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 mt-4">
                <div class="info-box">
                    <i class="bx bx-envelope"></i>
                    <h3>Notre E-mail</h3>
                    <p>info@example.com<br>contact@example.com</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 mt-4">
                <div class="info-box">
                    <i class="bx bx-phone-call"></i>
                    <h3>Appelez-nous</h3>
                    <p>(+225) 27 22 543 201<br>(+225) 01 40 233 535</p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-12 mt-4">
                <form wire:submit.prevent="sendMessage" role="form" class="php-email-form">
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <input type="text" wire:model="name" class="form-control" id="name" placeholder="Votre Nom"/>
                            @error('name')
                            <div class="error">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="email" class="form-control" wire:model="email" id="email" placeholder="Votre Email"/>
                            @error('email')
                            <div class="error">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" wire:model="subject" id="subject" placeholder="Objet du message"/>
                        @error('subject')
                        <div class="error">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" wire:model="message" rows="5"></textarea>
                        @error('message')
                         <div class="error">{{$message}}</div>
                        @enderror
                    </div>
                    @if(session()->has('message_sent'))
                    <div class="mb-3">
                        <div class="alert alert-success">
                            Votre message a été envoyé. Merci!
                        </div>
                    </div>
                    @endif
                    <div class="text-center">
                        <button type="submit">Envoyez votre message</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>