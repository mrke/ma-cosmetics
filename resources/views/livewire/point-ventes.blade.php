<div>
    <x-slot name="title">Nos points de ventes</x-slot>

    <x-slot name="meta_description">
        Où trouver les savons de Marie ? cliquez-ici...
        Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="app_styles">
        <link href="{{asset('assets/css/app.css')}}" rel="stylesheet">
    </x-slot>
    <x-slot name="pv_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Points de vente des savons Marie Andréa Cosmétics</x-slot>
            <x-slot name="page_title">Nos points de ventes</x-slot>
        </x-other-page-banner>
    </x-slot>
    <section id="team" class="team" style="padding: 0">
        <div class="container">

            <div class="section-title">
                <h2 data-aos="fade-up">La liste de nos points de vente</h2>
                <p data-aos="fade-up"> </p>
            </div>

            <div class="row">
                @foreach($pventes as $pvente)
                    <x-pv-item :pvente="$pvente"/>
                @endforeach
            </div>
            {{$pventes->links()}}
        </div>
    </section>
    
</div>
