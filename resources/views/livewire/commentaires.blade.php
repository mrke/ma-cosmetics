<x-slot name="cmt_active">active</x-slot>
<div class="card-header">
    <h3 class="card-title">LISTE DES COMMENTAIRES</h3>
</div>
<div class="card-body">
    <div class="content">
        <div class="table-responsive border-top">
            <table class="table table-bordered table-hover mb-0 text-nowrap" id="cmt-table">
                <thead>
                    <tr>
                        <th>PRODUIT</th>
                        <th>AVIS DONNE PAR</th>
                        <th>DATE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($comments as $comment)
                        <tr>
                            <td>
                                {{$comment->name}}
                            </td>
                            <td>{{$comment->user_name}}</td>
                            <td>{{$comment->created_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#cmt-table").dataTable({
            responsive: true,
            processing: true,
            language: {
                url:  url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            }
        })
    })
</script>