<x-admin-layout>
    <x-slot name="p_active">active</x-slot>
    <x-slot name="content">
        <div class="card-header">
            <h3 class="card-title">Liste des produits</h3>
        </div>
        <div class="card-body">
        <form action="#" id="add-product" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <input type="text" value="{{$product_id ? $product->id : ''}}" name="id" style="display: none">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label class="form-label text-dark" for="product_image">Image Du Produit</label>
                    <input type="file" class="form-control" name="product_image" id="product_image" placeholder="Image du produit" required style="{{$product_id ? 'display: none' : ''}}">
                    <div class="image-container" >
                        <img src="{{$product_id ? $product->product_image : ''}}" class="product-image" alt="PRODUCT IMAGE" style="{{$product_id ? '' : 'display: none'}}">
                        <button type="button" class="btn btn-sm btn-danger mt-2" onclick="changeImage()">
                            <i class="bx bx-window-close"></i>
                        </button>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label class="form-label text-dark" for="name">Nom Du Produit</label>
                    <input type="text" value="{{$product_id ? $product->name : ''}}" class="form-control" name="name" id="name" placeholder="Nom du produit" required>
                </div>
            </div>

            <div class="form-group">
                <label class="form-label text-dark" for="description">Description Du Produit</label>
                <textarea
                    placeholder="Décrivez brievement ce produit"
                    name="description" id="description"
                    class="form-control" rows="10"
                    required>{{$product_id ? $product->description : ''}}</textarea>
            </div>

            <div class="row">
                <div class="form-group col-lg-6">
                    <label class="form-label text-dark" for="composition">Compositions Du Produit</label>
                    <input value="{{$product_id ? $product->composition : ''}}" type="text" class="form-control" name="composition" id="composition" placeholder="Quelles sont les compositions de ce produit ?" required>
                </div>

                <div class="form-group col-lg-6">
                    <label class="form-label text-dark" for="vertus">Vertus Du Produits</label>
                    <input value="{{$product_id ? $product->vertus : ''}}" type="text" class="form-control" name="vertus" id="vertus" placeholder="Quelles sont les vertus de ce produit ?" required>
                </div>
            </div>

            <div class="form-group">
                <label class="form-label text-dark" for="tarif">Variation Et Tarif Associé</label>
                <div class="row">
                    @foreach($variations as $variation)
                        <div class="col-lg-6 d-flex">
                            <label
                                class="custom-control custom-checkbox mb-2"
                            >
                                <input
                                    type="checkbox"
                                    class="custom-control-input w-100"
                                    value="{{$variation->id}}"
                                    id="variation-{{$variation->id}}"
                                    onclick="showPriceField({{$variation->id}})"
                                    name="variations[]"
                                    {{$product_id ? 'checked' : ''}}
                                />
                                <span
                                    class="custom-control-label"
                                >{{$variation->variation}}</span
                                >
                            </label>
                           @if($product_id)
                            @foreach($tarifs as $tarif)
                                @if($variation->id === $tarif->variation_id)
                                    <input type="number" value="{{$tarif->price}}" class="form-control" min="0" name="tarif-{{$variation->id}}"
                                           id="tarif-{{$variation->id}}"
                                           placeholder="Entrer le prix"
                                           required
                                    >
                                @endif
                            @endforeach
                            @else
                                <input type="number" class="form-control" min="0" name="tarif-{{$variation->id}}"
                                       id="tarif-{{$variation->id}}"
                                       placeholder="Entrer le prix"
                                       required
                                       style="{{$product_id ? '' : 'display: none'}}"
                                >
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="form-group text-right">
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-save mr-1"></i>Sauvegarder
                </button>
            </div>

        </form>
        </div>

    </x-slot>
</x-admin-layout>

<script type="text/javascript">
    $(function () {
        $("#description").summernote({
            lang: 'fr-FR'
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        const form = $("#add-product");

        if (form.length > 0) {
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };

            form.validate({
                submitHandler: function (form) {
                    $("button[type='submit']").text("Envoie en cours...");
                    $.ajax({
                        data: new FormData(form),
                        dataType: 'JSON',
                        contentType: false,
                        cache: false,
                        processData: false,
                        type: 'POST',
                        url: '{{route('product.store',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}',
                        success: function (data) {
                            console.log(data)
                            $("button[type='submit']").text("Sauvegarder");
                            $("#add-product").get(0).reset();
                            $("#description").summernote('code', '');
                            $("#articleModal").modal('hide');
                            const PTable = $("#product-table").dataTable();
                            PTable.fnDraw(false);
                            toastr.success('Product créé et mise en ligne avec succès','OPERATION REUSSIE');

                            setTimeout(() =>{
                                window.location.href = '{{route('admin.products',['role'=>App\Models\Role::find(auth()->user()->role_id)->role])}}';
                            }, Math.floor(Math.random()*6000));
                        },
                        error: function (data) {
                            console.log(data)
                            switch (data.status) {
                                case 422:
                                    toastr.error("Un ou plusieurs champs n'ont pas été correctement remplis, rééssayer", "ERREUR CHAMPS");
                                    break;
                                case 500:
                                    toastr.error("Error lors de la communication avec le serveur", "ERREUR SERVEUR");
                                    break;
                                default:
                                    toastr.warning("Quelque s'est mal passé, veuillez rééssayer plus tard", "ERREUR INCONNUE");
                            }

                            $("button[type='submit']").text("Sauvegarder");

                        }
                    })
                }
            });
        }
    });


   // ("input[type='number']").hide()
    function showPriceField(id) {
        if(document.getElementById('variation-'+id).checked) {
            $("#tarif-"+id).show();
        }else{
            $("#tarif-"+id).hide();
        }
    }

    function changeImage() {
        $("input#product_image").show();
        $(".image-container").hide();
    }
</script>
