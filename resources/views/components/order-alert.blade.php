<div class="container" id="order-alert">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="bx bxs-info mr-2"></i> 
    Vous avez un processus de commande non achévé, 
    <a href="{{route('shopping.myCart')}}">cliquez-ici</a> pour continuer.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
</div>