@props(['product', 'tarifs'])
<div class="card">
    <div class="card-img">
        <img src="{{$product->product_image}}" alt="Marie Andréa Cosmétics - {{$product->name}}">
    </div>
    <div class="card-body">
        <h5 class="card-title"><a href="{{route('product.details',['name'=>\App\Http\Repositories\AppRepository::formatURL($product->name),'id'=>$product->id])}}">{{substr($product->name, 0, 11)}}...</a></h5>
        <div class="item-card-desc d-flex mb-2">
            <a href="#">
                <i class="bx bxs-dollar-sign text-muted mr-2"></i>
                {{\App\Http\Repositories\AppRepository::triePrice($tarifs,$product->id)}}
            </a>
        </div>
        <p class="card-text">
        <p>
           <b class="text-danger">Composition</b>: {{$product->composition}} <br>
           <b class="text-danger">Vertus & Propriété</b>: {{$product->vertus}}
         </p>
        </p>
        <div class="read-more">
            <a href="{{route('product.details',['name'=>\App\Http\Repositories\AppRepository::formatURL($product->name),'id'=>$product->id])}}">
                <i class="icofont-arrow-right"></i>
                Voir plus
           </a>
        </div>
    </div>
</div>
