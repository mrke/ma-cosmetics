
      @props(['product'])
      <div class="reply-form">
        <h4>Laisser un commentaire sur ce savons</h4>
        <form autocomplete="off" wire:submit.prevent="addComment">
          <div class="row">
            <div class="col-md-6 form-group">
              <input wire:model="fullname" type="{{auth()->check() ? 'hidden' : 'text'}}" value="{{auth()->check() ? auth()->user()->fullname : ''}}" class="form-control" placeholder="Votre nom*">
            </div>
            <div class="col-md-6 form-group">
              <input wire:model="email" type="{{auth()->check() ? 'hidden' : 'email'}}" value="{{auth()->check() ? auth()->user()->email : ''}}" class="form-control" placeholder="Votre email*">
              @error('email')
                <div class="error">{{$message}}</div>
              @enderror
            </div>
          </div>
          <div class="row">
            <div class="col form-group">
              <textarea wire:model="message" class="form-control" placeholder="Votre commentaire*"></textarea>
              @error('message')
                <div class="error">{{$message}}</div>
              @enderror
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Poster le commentaire</button>
        </form>
      </div>