@props(['product', 'tarifs','comments'])

<div class="col-lg-8 entries">
    <article class="entry entry-single">
      <div class="entry-img">
        <img src="{{$product->product_image}}" alt="Marie Andréa Cosmétics - {{$product->name}}" class="img-fluid">
      </div>

      <h2 class="entry-title">
        <a href="#">{{$product->name}}</a>
      </h2>

      <div class="entry-meta">
        <ul>
          <li class="d-flex align-items-center"><i class="icofont-cart"></i> <a href="blog-single.html">En stock illimité</a></li>
          <li class="d-flex align-items-center"><i class="icofont-comment"></i> <a href="blog-single.html">{{count($comments)}} Commentaires</a></li>
        </ul>
      </div>
      <h3>Détails du produit.</h3>
      <div class="entry-content">
       
        {!!$product->description!!}

        <blockquote>
          <i class="icofont-quote-left quote-left"></i>
          <p>
            {{$product->vertus}} <br>
            {{$product->composition}}
          </p>
          <i class="las la-quote-right quote-right"></i>
          <i class="icofont-quote-right quote-right"></i>
        </blockquote>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table row table-borderless w100 m-0 text-nowrap">
                        <tbody class="col-lg-6">
                        @foreach($tarifs as $tarif)
                            <tr>
                                <td class="w-150 px-0">
                                    <span class="font-weight-semibold">{{$tarif->variation}}</span>
                            </td>
                            <td><span>:</span></td>
                            <td style="color: red"><span>{{$tarif->price}} F CFA</span></td>
                        </tr>
                     @endforeach
                    </tbody>
                </table>
            </div>
        </div>

      </div>

      <div class="entry-footer clearfix">
        <div class="float-left">
          <i class="icofont-cart"></i>
          <ul class="cats">
            <li><a href="{{route('shopping.add',['id'=>$product->id])}}">Acheter ce savon</a></li>
          </ul>
        </div>
      </div>

    </article>

    <div class="blog-comments">

      <h4 class="comments-count">{{ count($comments) === 0 ? 'Aucun commentaire' : count($comments)." Commentaires"}}</h4>

      @foreach ($comments as $comment)
       <x-comment-item :comment="$comment"/>
      @endforeach
   

      @if(session()->has('commented'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <i class="bx bxs-check-circle mr-2"></i> {{session('commented')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      <x-post-comment :product="$product"/>
    </div>
  </div>
  