@props(['products'])
<div class="col-lg-4">
    <div class="sidebar">
      <h3 class="sidebar-title">Chercher</h3>
      <div class="sidebar-item search-form">
        <form wire:submit.prevent="searchProduct">
          <input type="text" wire:keyup="searchProduct" wire:model="q">
          <button type="submit"><i class="icofont-search"></i></button>
        </form>
      </div>
      <h3 class="sidebar-title">Voir d'autres savons</h3>
      <div class="sidebar-item recent-posts">
        @if(count($products) === 0)
        Aucun produit...
        @else
        @foreach ($products as $product)
        <div class="post-item clearfix">
          <img src="{{$product->product_image}}" alt="">
          <h4><a href="{{route('product.details',['name'=>\App\Http\Repositories\AppRepository::formatURL($product->name),'id'=>$product->id])}}">{{$product->name}}</a></h4>
          <span class="other-product prices">
            {{
              \App\Http\Repositories\AppRepository::triePrice(
                App\Models\Tarif::query()->join('variations','tarifs.variation_id','=','variations.id')
            ->select('tarifs.*','variations.variation as variation')->get(),
                $product->id
                )
              }}
          </span>
        </div>
        @endforeach
        @endif
      </div>           
    </div>
</div>