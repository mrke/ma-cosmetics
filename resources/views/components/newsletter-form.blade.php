<form action="javascript:void(0)" class="newletterForm" autocomplete="off">
    @csrf
    <input type="email" placeholder="Entrer votre Email" name="email">
    <input type="submit" value="Souscrire">
</form>
<div class="alert-newsletter"></div>

<script>
    !(function ($) {
        'use strict'
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        const newsletterForm = $('.newletterForm');
        if(newsletterForm.length > 0) {
            newsletterForm.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    data: newsletterForm.serialize(),
                    type: 'POST',
                    url: '{{route('storeEmail')}}',
                    dataType: 'JSON',
                    cache: false,
                    success: function (data) {
                        newsletterForm.get(0).reset();
                        $('.alert-newsletter').addClass('alert-success')
                            .html(` <i class="fa fa-check-circle"></i> Merci pour votre inscription à notre lettre d'informations`)
                            .show('slow').delay(5000).hide('slow');
                    },
                    error: function (data) {
                        let message;
                        if (data.responseJSON.email[0] === 'validation.required'){
                            message = 'Ce champ est obligatoire';
                        } else if (data.responseJSON.email[0] === 'validation.unique'){
                            message = 'Cette adresse email existe déjà dans nos enregistrements.';
                        } else if (data.responseJSON.email[0] === 'validation.email'){
                            message = 'La valeur du champ doit être une adresse email.';
                        } else {
                            message = 'Quelque chose s\'est mal passé, veuillez rééssayer';
                            console.log(data)
                        }
                        $('.alert-newsletter-danger').addClass('alert-danger')
                            .html(`<i class="fa fa-exclamation-triangle mr-2"></i>`+message)
                            .show('slow').delay(5000).hide('slow');
                    }
                })
            });
        }

    })
</script>
