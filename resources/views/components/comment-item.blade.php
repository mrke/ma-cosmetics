@props(['comment'])

<div id="comment-4" class="comment clearfix">
    <img src="assets/img/comments-6.jpg" class="comment-img  float-left" alt="">
    <h5><a href="#">{{$comment->name}}</a>
    <time datetime="2020-01-01">{{
      (new \Carbon\Carbon($comment->created_at))->diffForHumans()
      }}</time>
    <p>
      {{$comment->message}}
    </p>
</div>