@props(['variations'])
@foreach($variations as $variation)
    <div class="col-lg-12 d-flex">
        <label
            class="custom-control custom-checkbox mb-2 d-flex w-40"
        >
            <input
                type="checkbox"
                class="custom-control-input w-100 variations"
                value="{{$variation->variation_id}}"
                id="variation-{{$variation->variation_id}}"
                onclick="showQuantityField({{$variation->variation_id}})"
                name="variations[]"
            />
            <span
                class="custom-control-label"
            >{{$variation->variation}}</span
            ><span class="mr-1 ml-1">:</span><span class="text-danger">{{$variation->price}} F CFA</span>
        </label>
        <input type="number" class="form-control w-60 mb-2" min="1" name="quantity-{{$variation->variation_id}}"
               id="quantity-{{$variation->variation_id}}"
               placeholder="Entrer la quantité"
               required
        >
    </div>
@endforeach