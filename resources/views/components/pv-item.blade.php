@props(['pvente'])
<div class="col-lg-6 d-flex align-items-stretch">
    <div class="card overflow-hidden br-0">
        <div class="card-body d-sm-flex p-3">
            <div class="p-0 mr-3">
                <div class="">
                    <a href="#"></a>
                    <img class="w-9 h-9" src="{{asset('assets/img/logo.png')}}"  alt="LOGO PARTENAIRE">
                </div>
            </div>
            <div class="mt-3 mt-md-5 item-card">
                <a href="#" class="text-dark">
                    <h4 class="font-weight-semibold mt-0">{{$pvente->nom_pharmacie}}</h4>
                </a>
                <div class="rating-stars d-inline-flex">
                    <div class="rating-stars-container mr-2">
                        <div class="rating-star sm is-active">
                            <i class="bx bxs-star"></i>
                        </div>
                        <div class="rating-star sm is-active">
                            <i class="bx bxs-star"></i>
                        </div>
                        <div class="rating-star sm is-active">
                            <i class="bx bxs-star"></i>
                        </div>
                        <div class="rating-star sm is-active">
                            <i class="bx bxs-star"></i>
                        </div>
                        <div class="rating-star sm">
                            <i class="bx bxs-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card overflow-hidden box-shadow-0 br-0 border-0 mb-0" id="pv-details-{{$pvente->id}}">
            <div class="card-body table-responsive border-top">
                <table class="table table-borderless w-100 m-0 text-wrap">
                    <tbody class="p-0">
                    <tr>
                        <td class="pl-0 pb-1">
                            <span class="font-weight-semibold">Localisation</span>
                        </td>
                        <td class="p-1"><span>:</span></td>
                        <td class="p-1"><span>{{$pvente->localisation}}</span></td>
                    </tr>
                    <tr>
                        <td class="pl-0 pb-1">
                            <span class="font-weight-semibold">Adresse</span>
                        </td>
                        <td class="p-1"><span>:</span></td>
                        <td class="p-1"><span>{{$pvente->address}}</span></td>
                    </tr>
                    <tr>
                        <td class="pl-0 pb-1">
                            <span class="font-weight-semibold">Contacts</span>
                        </td>
                        <td class="p-1"><span>:</span></td>
                        <td class="p-1"><span>{{$pvente->contacts}}</span></td>
                    </tr>
                    <tr>
                        <td class="pl-0 pb-1">
                            <span class="font-weight-semibold">Email</span>
                        </td>
                        <td class="p-1"><span>:</span></td>
                        <td class="p-1"><span>{{$pvente->email}}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
