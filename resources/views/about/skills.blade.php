<section class="skills" data-aos="fade-up">
    <div class="container">

        <div class="section-title">
            <h2>Nos compétences</h2>
            <p></p>
        </div>

        <div class="skills-content">

            <div class="progress">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                    <span class="skill">fabrication de nos savons artisanaux <i class="val">100%</i></span>
                </div>
            </div>

            <div class="progress">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                    <span class="skill">Saponification à froid <i class="val">90%</i></span>
                </div>
            </div>

            <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                    <span class="skill">Soins de peau et votre santé. <i class="val">75%</i></span>
                </div>
            </div>

        </div>

    </div>
</section>
