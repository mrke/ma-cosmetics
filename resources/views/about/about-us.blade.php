<section class="about" data-aos="fade-up">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                <img src="{{asset('assets/img/about-img.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0">
                <h3>Qui sommes-nous ?</h3>
                <p class="font-italic">
                    Marie Andréa SARL vous propose du savon artisanal sous toutes
                    ses formes et des produits cosmétiques à base de produits naturels.
                </p>
                <ul>
                    <li><i class="icofont-check-circled"></i> Ouvert de 09h à 17h30.</li>
                    <li><i class="icofont-check-circled"></i> II plateaux après le centre commercial sococe</li>
                    <li><i class="icofont-check-circled"></i> Devant la boulangerie le Roi du pain</li>
                </ul>
                <p>
                    Concernant la fabrication de nos savons artisanaux nous appliquons la
                    saponification à froid, une méthode traditionnelle et artisanale permettant
                    d’élaborer des savons uniques, riches en huile végétale (huile de palmiste et le beurre de karité),
                    qui sont de véritables soins protecteurs pour votre peau et votre santé.
                </p>
            </div>
        </div>

    </div>
</section>
