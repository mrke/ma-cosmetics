<section class="facts section-bg" data-aos="fade-up">
    <div class="container">

        <div class="row counters">

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">1500</span>
                <p>Clients</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">{{count(App\Models\PointDeVente::all())}}</span>
                <p>Points de ventes</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">02</span>
                <p>Partenaires</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">1400</span>
                <p>Clients satisfaits</p>
            </div>

        </div>

    </div>
</section>
