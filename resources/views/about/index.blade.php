<x-ma-cosmetics-layout>

    <x-slot name="title">Qui sommes nous ?</x-slot>

    <x-slot name="meta_description">
        Présentation de Marie Andréa Cosmétics ! a propos ? Meilleure entreprise de savon naturel et bio !
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        a propos ? Meilleure entreprise de savon naturel et bio !
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="about_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Qui sommes nous ?</x-slot>
            <x-slot name="page_title">A propos de Marie Andréa Cosmétics</x-slot>
        </x-other-page-banner>
    </x-slot>

    <x-slot name="content">
        @include('about.about-us')
        @include('about.counter-section')
        @include('about.skills')
    </x-slot>

</x-ma-cosmetics-layout>
