<x-ma-cosmetics-layout>
<x-slot name="title">Commande envoyé</x-slot>

    <x-slot name="meta_description">
        Marie Andréa cosmétics - Entreprise de fabrication de savon bio, Vente de savon bio, Vente de savon 100% naturel
    </x-slot>

    <x-slot name="meta_keywords">
        Marie Andréa cosmétics, beauté, savon bio, savon naturel, 100% bio, 100% naturel
    </x-slot>
    <x-slot name="product_class">active</x-slot>

    <x-slot name="banner">
        <x-other-page-banner>
            <x-slot name="page"> Catalogue des savons Marie Andréa Cosmétics</x-slot>
            <x-slot name="page_title">Commande envoyée</x-slot>
        </x-other-page-banner>
    </x-slot>
    <x-slot name="content">
    <section class="blog">
        <div class="container">
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 entries">
                <article class="entry entry-single">
                    <h4 style="color: #0A4F19">
                        <i class="fa fa-check mr-1"></i>
                        <span>Bonjour
                           @if(auth()->check())
                                très cher(e) {{ \App\Models\Client::where('user_id',auth()->id())->first()->firstName.' '.\App\Models\Client::where('user_id',auth()->id())->first()->lastname }}
                            @else
                               Cher client
                            @endif
                       , </span>
                    </h4>
                    <p style="color: #383d41">
                        Votre commade chez <b style="color: red">Marie Andréa Cosmétics</b> a été receptionnée avec succès, nous vous contacterons
                            d'ici peu pour vous donner plus d'informations concernant la livraison de votre colis !
                        Merci pour votre fidélité.
                    </p>
                </article>
                <div class="p-4 mb-5">
                    <div class="btn-list d-sm-flex">
                        <a href="{{route('home')}}" class="btn btn-primary mb-sm-0 mr-1">
                            <i class="bx bxs-home mr-1"></i> Retourner à l'accueil
                        </a>
                        <a href="{{route('contact')}}" class="btn btn-secondary mb-sm-0 mr-1">
                            <i class="bx bxs-envelope ml-1"></i> Nous contactez
                        </a>
                        <a href="{{route('dashboard',['role'=> App\Models\Role::find(auth()->user()->role_id)->role])}}" class="btn btn-info mb-sm-0">
                             Voir la liste de mes commandes <i class="bx bxs-chevron-right ml-1"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
          </div>
        </div>
    </section>
    </x-slot>
</x-ma-cosmetics-layout>