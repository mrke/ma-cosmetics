<header id="header">
    <div class="container d-flex">

        <div class="logo mr-auto">
            <a href="{{route('home')}}"><img src="{{asset('assets/img/logo.png')}}" alt="LOGO - MARIE ANDREA COSMETICS" class="img-fluid"></a>
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="{{$active_class ?? ''}}"><a href="{{route('home')}}">Accueil</a></li>
                <li class="{{$about_class ?? ''}}"><a href="{{route('about')}}">Présenation</a></li>
                <li class="{{$product_class ?? ''}}"><a href="{{route('product')}}">Nos produits</a></li>
                <li class="{{$pv_class ?? ''}}"><a href="{{route('pvente')}}">Nos points de ventes</a></li>
                <li class="{{$actu_class ?? ''}}"><a href="#">Actualités</a></li>
                <li class="{{$contact_class ?? ''}}"><a href="{{route('contact')}}">Contact</a></li>
            </ul>
        </nav>

    </div>
</header>
