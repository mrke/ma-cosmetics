<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>Marie Andréa Cosmétics</h3>
                    <p>
                        Situé aux 2 plateaux <br>
                        Centre commercial sococe <br>
                        Prochain carrefour à droite <br>
                        Direction en allant vers angré <br>
                        Devant la boulangerie le Roi du pain<br>
                        Marie Andréa SARL est à 150m, à la villa 45 <br><br>
                        <strong>Tel:</strong> (+225) 27 22 543 201  / 01 40 233 535<br>
                        <strong>E-mail:</strong> marieandrea.sarl20@gmail.com<br>
                    </p>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Liens utiles</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('home')}}">Accueil</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('about')}}">A propos</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('product')}}">Nos produits</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('pvente')}}">Points de ventes</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('register')}}">Créer un compte</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Nos services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Fabrication de savon Bio</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Vente en détails</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Vente en gros</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Livraison</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Rejoignez Notre Newsletter</h4>
                    <p></p>
                    <x-newsletter-form />
                </div>

            </div>
        </div>
    </div>

    <div class="container d-lg-flex py-4">

        <div class="mr-lg-auto text-center text-lg-left">
            <div class="copyright">
                &copy; Copyright <strong><span>Marie Andréa Cosmétics</span></strong>. Tous droits sont réservés.
            </div>
            <div class="credits">
                Designed by <a href="mailto:ernestkouassi02@gmail.com">Ernesr KOUASSI</a>
            </div>
        </div>
        <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
    </div>
</footer>
