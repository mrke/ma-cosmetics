<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <ul>
                <li><i class="icofont-envelope"></i><a href="mailto:marieandrea.sarl20@gmail.com">marieandrea.sarl20@gmail.com</a></li>
                <li><i class="icofont-phone"></i> (+225) 27 22 54 32 01 / 01 40 233 535</li>
                <li><i class="icofont-clock-time icofont-flip-horizontal"></i> 
                    <span id="ma_custom-date"></span>
                </li>
            </ul>

        </div>
        @if(auth()->check())
            <div class="auth">
                <a href="javascript:void(0)">
                    <img class="user-avatar"
                     src="{{auth()->user()->avatar ? auth()->user()->avatar :'/assets/uploads/users/default.jpg'}}" alt="Marie Andréa - Utilisateur">
                     <span class="user-name">
                         {{
                            auth()->user()->fullName
                            ?
                            auth()->user()->fullName
                            :
                            \App\Models\Role::find(auth()->user()->role_id)->role
                        }}
                    </span>
                </a>
                <div class="auth-menu">
                    <a href="{{route('dashboard',['role' => App\Models\Role::find(auth()->user()->role_id)->role])}}">
                        <i class="bx bxs-shopping-bags"></i>
                        {{App\Models\Role::find(auth()->user()->role_id)->role === 'customer' ? 'Mes commandes' : 'Tableau de bord'}}
                    </a>
                    <a href="{{route('logout')}}"><i class="bx bxs-log-out"></i> Me déconnecter</a>
                </div>
            </div>
        @else
        <div class="cta">
            <a href="{{route('login')}}">
                <i class="bx bxs-lock"></i>
                Se connecter
            </a>
        </div>
        @endif
    </div>
</section>
