!(function($) {
    "use strict";

    // Smooth scroll for the navigation menu and links with .scrollto classes
    $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function(e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            e.preventDefault();
            var target = $(this.hash);
            if (target.length) {

                var scrollto = target.offset().top;
                var scrolled = 0;

                if ($('#header').length) {
                    scrollto -= $('#header').outerHeight()

                    if (!$('#header').hasClass('header-scrolled')) {
                        scrollto += scrolled;
                    }
                }

                if ($(this).attr("href") == '#header') {
                    scrollto = 0;
                }

                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu, .mobile-nav').length) {
                    $('.nav-menu .active, .mobile-nav .active').removeClass('active');
                    $(this).closest('li').addClass('active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
                return false;
            }
        }
    });

    // Mobile Navigation
    if ($('.nav-menu').length) {
        var $mobile_nav = $('.nav-menu').clone().prop({
            class: 'mobile-nav d-lg-none'
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
        $('body').append('<div class="mobile-nav-overly"></div>');

        $(document).on('click', '.mobile-nav-toggle', function(e) {
            $('body').toggleClass('mobile-nav-active');
            $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
            $('.mobile-nav-overly').toggle();
        });

        $(document).on('click', '.mobile-nav .drop-down > a', function(e) {
            e.preventDefault();
            $(this).next().slideToggle(300);
            $(this).parent().toggleClass('active');
        });

        $(document).click(function(e) {
            var container = $(".mobile-nav, .mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
            }
        });
    } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
        $(".mobile-nav, .mobile-nav-toggle").hide();
    }

    // Toggle .header-scrolled class to #header when page is scrolled
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
        } else {
            $('#header').removeClass('header-scrolled');
        }
    });

    if ($(window).scrollTop() > 100) {
        $('#header').addClass('header-scrolled');
    }

    // Stick the header at top on scroll
    $("#header").sticky({
        topSpacing: 0,
        zIndex: '50'
    });

    // Back to top button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });
    // Initiate the venobox plugin
    $(window).on('load', function() {
        $('.venobox').venobox();
    });

    // Clients carousel (uses the Owl Carousel library)
    $(".clients-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 4
            },
            900: {
                items: 6
            }
        }
    });

    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        items: 1
    });

    // Porfolio isotope and filter
    $(window).on('load', function() {
        var portfolioIsotope = $('.portfolio-container').isotope({
            itemSelector: '.portfolio-item',
            layoutMode: 'fitRows'
        });

        $('#portfolio-flters li').on('click', function() {
            $("#portfolio-flters li").removeClass('filter-active');
            $(this).addClass('filter-active');

            portfolioIsotope.isotope({
                filter: $(this).data('filter')
            });
        });

        // Initiate venobox (lightbox feature used in portofilo)
        $(document).ready(function() {
            $('.venobox').venobox();
        });
    });

    // Initi AOS
    AOS.init({
        duration: 1000,
        easing: "ease-in-out-back"
    });

    $('.skills-content').waypoint(function() {
        $('.progress .progress-bar').each(function() {
            $(this).css("width", $(this).attr("aria-valuenow") + '%');
        });
    }, {
        offset: '80%'
    });

    $("nav[role='navigation ']> .relative.z-0.inline-flex.rounded-md.shadow-sm > span > button")
        .addClass("page-link");

    // MY ADD TO CART LOGIC

    $("form#addToCartForm input[type='number']").hide();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
        }
    });

    // AJOUTER DES PRODUITS AU PANIER AVANT DE VALIDER LA COMMANDE
    const form = $("#addToCartForm");
    if (form.length > 0) {
        form.validate({
            submitHandler: function(form) {
                $("#addToCartForm button[type='submit']").text("Envoie en cours...");
                $.ajax({
                    data: new FormData(form),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    url: '/add-to-cart/store',
                    type: 'POST',
                    success: data => {
                        console.log(data)
                        let myData = null;
                        const lData = localStorage.getItem('myLocalCart');

                        if (lData === null) {
                            myData = data;
                            localStorage.setItem('myLocalCart', JSON.stringify(myData));
                        } else {
                            myData = JSON.parse(lData);
                            myData.push(...data)
                            localStorage.setItem('myLocalCart', JSON.stringify(myData));
                        }

                        toastr.success('Produit ajouté avec succès', 'MON PANIER');
                        $("#addToCartForm").get(0).reset();
                        $("#addToCartForm button[type='submit']").html(
                            `<i class="bx bxs-check-circle"></i> Valider`)
                    },
                    error: data => {
                        data.status === 422 ?
                            toastr.error("Vous devez impérativement choisir un poids") :
                            toastr.info("Quelque chose s'est passé, veuillez rééssayer !");

                        $("button[type='submit']").html(
                            `<i class="bx bxs-check-circle"></i>Valider`)
                    }
                })
            }
        })
    }

    // ALERTE POUR SIGNALER UNE COMMANDE NON ACHEVEE.
    $("#order-alert").hide();

    const orderData = JSON.parse(localStorage.getItem('myLocalCart'));
    let orderEl = 'Aucun produit selectionné';
    if (orderData) {
        orderData.map(el => {
            orderEl += `
                <tr>
                    <td>${el.produc_name}</td>
                    <td>${el.variation}</td>
                    <td>${el.quantity}</td>
                </tr>
            `
        });
    }

    $("#sent_order_table>tbody").html(orderEl);


    // SAVE ORDER AFTER CUSTOMER CLICK ON SENT BUTTON

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //const form = $("#register");

    $("#sent-order-btn").on('click', function(e) {
        e.preventDefault();
        const token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'POST',
            url: '/sent-order',
            dataType: 'JSON',
            data: {
                _token: token,
                ordered: JSON.parse(localStorage.getItem('myLocalCart')),
            },
            success: data => {
                localStorage.removeItem('myLocalCart');
                //$("#register").get(0).reset();
                console.log(data);
                window.location.href = '/commander/terminer/success'
            },
            error: data => {
                console.log(data);
                if (data.status === 422) {
                    toastr.error("Désolé, il y a des champs obligatoires à remplir", "ERREUR CHAMPS")
                } else {
                    toastr.error("Quelque chose s'est mal passée, veuillez rééssayer plus tard", "ERREUR TEMPORAIRE");
                    console.log(data);
                }
            }
        })
    })



})(jQuery);

function date() {
    const days = ['Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.', 'Dim.'];
    const months = ['Jan.', 'Fév.', 'Mars', 'Avr.', 'Mai', 'Jui.', 'Juil.', 'Aoû.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
    const date = new Date();

    let day = days[date.getDay() - 1];
    let date_ = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let month = months[date.getMonth()];
    let year = date.getFullYear();
    let hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    let min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    let sec = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

    const result = day +
        ' ' + date_ + ' ' +
        month + ' ' + year + ' ' +
        hour +
        ':' + min + ':' + sec;

    document.getElementById('ma_custom-date').innerHTML = result;
}


function showQuantityField(id) {
    if (document.getElementById(`variation-${id}`).checked) {
        $(`#quantity-${id}`).show();
    } else {
        $("#quantity-" + id).hide();
    }
}

function remplirPanier() {
    const lData = localStorage.getItem('myLocalCart');
    let htmlEl = '';
    if (lData) {
        const data = JSON.parse(lData);
        $("#order-alert").show();
        data.map((el, i) => {
            htmlEl += `
            <article class="entry entry-single">
                <div class="product">
                    ${el.produc_name} - ${el.variation}
                </div>
                <div class="product-quantity">
                    Quantité : ${el.quantity}
                </div> 
                <div class="product-remove-btn">
                <button onclick="deleteElementOnCart(${i})" class="icon-btn">&times;</button>
                </div>  
            </article>`
        });

        htmlEl += `<a class="btn btn-primary" href="/connexion?next_order=yes">Finaliser ma commande</a> <a class="btn btn-success" href="/contactez-nous">Je prefère laisser un message</a>`
    } else {
        htmlEl = `<article class="entry">Aucun article dans votre panier, <a href="/nos-produits">Cliquez-ici</a> pour en ajouter.</article>`
    }

    $("#my-cart-container").html(htmlEl);
}

remplirPanier();


function deleteElementOnCart(id) {
    if (confirm("Êtes-vous sûr de vouloir retirer cette ligne ?")) {
        const lData = localStorage.getItem('myLocalCart');
        const data = JSON.parse(lData).filter((el, i) => i !== id);
        //console.log(data);
        data.length > 1 ?
            localStorage.setItem('myLocalCart', JSON.stringify(data)) :
            localStorage.removeItem('myLocalCart')

        remplirPanier();
    }
}


setInterval(() => {
    date()
}, 1000);